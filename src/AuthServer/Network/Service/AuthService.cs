﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using BlubLib.DotNetty.Handlers.MessageHandling;
using BlubLib.Security.Cryptography;
using Dapper.FastCrud;
using TreasureSphere.Database.Auth;
using TreasureSphere.Network.Message.Auth;
using ProudNetSrc;
using ProudNetSrc.Handlers;
using Serilog;
using Serilog.Core;

namespace TreasureSphere.Network.Service
{
    internal class AuthService : ProudMessageHandler
    {
        // ReSharper disable once InconsistentNaming
        private static readonly ILogger Logger =
            Log.ForContext(Constants.SourceContextPropertyName, nameof(AuthService));
        
        [MessageHandler(typeof(LoginKRReqMessage))]
        public async Task KRLoginHandler(ProudSession session, LoginKRReqMessage message)
        {
        }

        [MessageHandler(typeof(LoginEUReqMessage))]
        public async Task EULoginHandler(ProudSession session, LoginEUReqMessage message)
        {
            var ip = session.RemoteEndPoint.Address.ToString();
            Logger.Debug($"Login from {ip} with username {message.Username}");

            AccountDto account;
            AccountDto account2;
            using (var db = AuthDatabase.Open())
            {
                var result = await db.FindAsync<AccountDto>(statement => statement
                        .Where($"{nameof(AccountDto.Username):C} = @{nameof(message.Username)}")
                        .Include<BanDto>(join => join.LeftOuterJoin())
                        .WithParameters(new { message.Username }));

                var result2 = await db.FindAsync<AccountDto>(statement => statement
                        .Where($"{nameof(AccountDto.Nickname):C} = @{nameof(message.Username)}")
                        .Include<BanDto>(join => join.LeftOuterJoin())
                        .WithParameters(new { message.Username }));

                account = result.FirstOrDefault();
                account2 = result2.FirstOrDefault();

                if (account == null && account2 == null)
                {
                    if (Config.Instance.NoobMode || Config.Instance.AutoRegister)
                    {
                        // NoobMode/AutoRegister: Create a new account if non exists
                        account = new AccountDto { Username = message.Username };

                        var newSalt = new byte[24];
                        using (var csprng = new RNGCryptoServiceProvider())
                            csprng.GetBytes(newSalt);

                        var hash = new byte[24];
                        using (var pbkdf2 = new Rfc2898DeriveBytes(message.Password, newSalt, 24000))
                            hash = pbkdf2.GetBytes(24);

                        account.Password = Convert.ToBase64String(hash);
                        account.Salt = Convert.ToBase64String(newSalt);

                        await db.InsertAsync(account);
                    }
                    else
                    {
                        Logger.Error($"Wrong login for {message.Username}");
                        session.SendAsync(new LoginEUAckMessage(AuthLoginResult.WrongIdorPw));
                        return;
                    }
                }

                if (account == null && account2 != null)
                    account = account2;
                
                var salt = Convert.FromBase64String(account.Salt);

                var passwordGuess = new byte[24];
                using (var pbkdf2 = new Rfc2898DeriveBytes(message.Password, salt, 24000))
                    passwordGuess = pbkdf2.GetBytes(24);

                var actualPassword = Convert.FromBase64String(account.Password);

                uint difference = (uint)passwordGuess.Length ^ (uint)actualPassword.Length;
                for (var i = 0; i < passwordGuess.Length && i < actualPassword.Length; i++)
                {
                    difference |= (uint)(passwordGuess[i] ^ actualPassword[i]);
                }

                if (difference != 0 || string.IsNullOrWhiteSpace(account.Password))
                {
                    if (Config.Instance.NoobMode)
                    {
                        // Noob Mode: Save new password
                        var newSalt = new byte[24];
                        using (var csprng = new RNGCryptoServiceProvider())
                            csprng.GetBytes(newSalt);

                        var hash = new byte[24];
                        using (var pbkdf2 = new Rfc2898DeriveBytes(message.Password, newSalt, 24000))
                            hash = pbkdf2.GetBytes(24);

                        account.Password = Convert.ToBase64String(hash);
                        account.Salt = Convert.ToBase64String(newSalt);

                        await db.UpdateAsync(account);
                    }
                    else
                    {
                        Logger.Error($"Wrong login for {message.Username}");
                        session.SendAsync(new LoginEUAckMessage(AuthLoginResult.WrongIdorPw));
                        return;
                    }
                }

                var now = DateTimeOffset.Now.ToUnixTimeSeconds();
                var ban = account.Bans.FirstOrDefault(b => b.Date + (b.Duration ?? 0) > now);
                if (ban != null)
                {
                    var unbanDate = DateTimeOffset.FromUnixTimeSeconds(ban.Date + (ban.Duration ?? 0));
                    Logger.Error($"{message.Username} is banned until {unbanDate}");
                    session.SendAsync(new LoginEUAckMessage(unbanDate));
                    return;
                }

                Logger.Information($"Login success for {message.Username}");

                var entry = new LoginHistoryDto
                {
                    AccountId = account.Id,
                    Date = DateTimeOffset.Now.ToUnixTimeSeconds(),
                    IP = ip
                };
                await db.InsertAsync(entry);

                account.LoginToken = $"{Hash.GetUInt32<CRC32>($"{DateTime.Now.ToFileTime()}")}";
                await db.UpdateAsync(account);
            }



            // ToDo proper session generation
            var sessionId = Hash.GetUInt32<CRC32>($"<{account.Username}+{account.Password}+{account.LoginToken}>");
            session.SendAsync(new LoginEUAckMessage(AuthLoginResult.OK, (ulong)account.Id, sessionId));
        }

        [MessageHandler(typeof(ServerListReqMessage))]
        public async Task ServerListHandler(AuthServer server, ProudSession session)
        {
            session.SendAsync(new ServerListAckMessage(server.ServerManager.ToArray()));
        }

        private static byte[] HexStringToByteArray(string hexString)
        {
            hexString = hexString.Replace("-", ""); // remove '-' symbols

            var result = new byte[hexString.Length / 2];

            for (var i = 0; i < hexString.Length; i += 2)
                result[i / 2] = Convert.ToByte(hexString.Substring(i, 2), 16); // base 16

            return result;
        }

        [MessageHandler(typeof(GameDataReqMessage))]
        public async Task DataHandler(AuthServer server, ProudSession session)
        {
            foreach (var xbn in Enum.GetValues(typeof(XBNType)).Cast<XBNType>().ToList())
            {
                if (Program.XBNdata.TryGetValue(xbn, out var xbninfo))
                {
                    var readoffset = 0;
                    while (readoffset != xbninfo.Length)
                    {
                        var size = xbninfo.Length - readoffset;

                        if (size > 40000)
                            size = 40000;

                        var data = new byte[size];
                        Array.Copy(xbninfo, readoffset, data, 0, size);

                        await session.SendAsync(new GameDataAckMessage((uint)xbn, data, (uint)xbninfo.Length), SendOptions.ReliableSecureCompress);
                        readoffset += size;
                    }
                }
            }
        }

    }
}
