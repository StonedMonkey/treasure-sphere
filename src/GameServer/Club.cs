﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Dapper.FastCrud;
using ExpressMapper.Extensions;
using TreasureSphere.Database.Auth;
using TreasureSphere.Database.Game;
using TreasureSphere.Network;
using TreasureSphere.Network.Data.Game;
using TreasureSphere.Network.Message.Game;
using Serilog;
using Serilog.Core;

namespace TreasureSphere
{
    internal class DBClubInfoDto
    {
        public ClubDto ClubDto { get; set; }
        public ClubPlayerInfo[] PlayerDto { get; set; }
    }

    internal class ClubPlayerInfo
    {
        public ulong AccountId { get; set; }
        public ClubState State { get; set; }
        public bool IsMod { get; set; }

        public AccountDto account { get; set; }
    }
    
    internal class Club
    {
        // ReSharper disable once InconsistentNaming
        private static readonly ILogger Logger = Log.ForContext(Constants.SourceContextPropertyName, nameof(Club));
        private readonly Dictionary<ulong, ClubPlayerInfo> _players = new Dictionary<ulong, ClubPlayerInfo>();
        internal bool NeedsToSave { get; set; }

        public Dictionary<ulong, ClubPlayerInfo> Players => _players;
        private uint _id;
        private string _name;
        private string _icon;
        
        public ClubPlayerInfo this[ulong id] => _players[id];
        public int Count => _players.Count;

        public Club()
        {
        }

        public Club(ClubDto dto, ClubPlayerInfo[] Player)
        {
            _players = Player.ToDictionary(playerinfo => playerinfo.AccountId);
            Clan_ID = dto.Id;
            Clan_Name = dto.Name;
            Clan_Icon = dto.Icon;

            Logger.Information("New Club: {name} {type} {playercount}", Clan_Name, Clan_Icon, Count);
        }

        public void Save()
        {
            using (var db = GameDatabase.Open())
            {
                if (NeedsToSave)
                {
                    db.Update(new ClubDto
                    {
                        Id = _id,
                        Name = _name,
                        Icon = _icon,
                    });
                    
                    NeedsToSave = false;
                }
            }
        }

        public uint Clan_ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Clan_Icon
        {
            get { return _icon; }
            set { _icon = value; }
        }

        public string Clan_Name
        {
            get { return _name; }
            set { _name = value; }
        } 
    }
}
