﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TreasureSphere;
using TreasureSphere.Network.Data.GameRule;
using TreasureSphere.Network.Message.GameRule;
using TreasureSphere.Game.Systems;

// ReSharper disable once CheckNamespace
namespace TreasureSphere.Game.GameRules
{
    internal class CaptainGameRule : GameRuleBase
    {
        
        public override bool CountMatch => true;
        public override GameRule GameRule => GameRule.Captain;
        public override Briefing Briefing { get; }
        public override void IntrudeCompleted(Player plr)
        {
            base.IntrudeCompleted(plr);
            plr.Session.SendAsync(new CaptainCurrentRoundInfoAckMessage(AlphaPoints, BetaPoints));
        }


        public int AlphaPoints { get; set; } = 0;
        public int BetaPoints { get; set; } = 0;
        public int Rounds { get; set; } = 0;

        public List<Player> AlphaCaptains = new List<Player>();
        public List<Player> BetaCaptains = new List<Player>();

        private bool _firstround = true;
        private bool _waitingNextRound;
        private TimeSpan _nextRoundTimer = TimeSpan.Zero;
        private static readonly TimeSpan s_nextRoundWaitTime = TimeSpan.FromSeconds(10);


        public CaptainGameRule(Room room)
            : base(room)
            
        {
            Briefing = new Briefing(this);

            StateMachine.Configure(GameRuleState.Waiting)
                .PermitIf(GameRuleStateTrigger.StartPrepare, GameRuleState.Preparing, CanStartGame);

            StateMachine.Configure(GameRuleState.Preparing)
                .Permit(GameRuleStateTrigger.StartGame, GameRuleState.FullGame);

            StateMachine.Configure(GameRuleState.FullGame)
                .SubstateOf(GameRuleState.Playing)
                .Permit(GameRuleStateTrigger.StartResult, GameRuleState.EnteringResult);

            StateMachine.Configure(GameRuleState.EnteringResult)
                .SubstateOf(GameRuleState.Playing)
                .Permit(GameRuleStateTrigger.StartResult, GameRuleState.Result);

            StateMachine.Configure(GameRuleState.Result)
                .SubstateOf(GameRuleState.Playing)
                .Permit(GameRuleStateTrigger.EndGame, GameRuleState.Waiting);
        }

        public override void Initialize()
        {
            var teamMgr = Room.TeamManager;
            teamMgr.Add(Team.Alpha, (uint)(Room.Options.PlayerLimit / 2), (uint)(Room.Options.Spectator / 2));
            teamMgr.Add(Team.Beta, (uint)(Room.Options.PlayerLimit / 2), (uint)(Room.Options.Spectator / 2));

            base.Initialize();
        }

        public override void Cleanup()
        {
            var teamMgr = Room.TeamManager;
            teamMgr.Remove(Team.Alpha);
            teamMgr.Remove(Team.Beta);

            AlphaPoints = 0;
            BetaPoints = 0;
            Rounds = 0;

            base.Cleanup();
        }

        public void NextRound()
        {
            Room.Broadcast(new CaptainCurrentRoundInfoAckMessage(AlphaPoints, BetaPoints));
            _nextRoundTimer = TimeSpan.Zero;
            _waitingNextRound = true;
        }

        public void StartRound()
        {
            AlphaCaptains = new List<Player>();
            BetaCaptains = new List<Player>();

            List<CaptainLifeDto> players = new List<CaptainLifeDto>();
            uint count = 0;
            foreach (Player plr in Room.TeamManager.PlayersPlaying)
            {
                count++;
                plr.RoomInfo.Team.Score = count;

                if (plr.RoomInfo.Team.Team == Team.Alpha)
                {
                    AlphaCaptains.Add(plr);
                }
                else if (plr.RoomInfo.Team.Team == Team.Beta)
                {
                    BetaCaptains.Add(plr);
                }

                //float max = (Team.Alpha.Count() > _beta.Count()) ? _alpha.Count() : _beta.Count();
                players.Add(new CaptainLifeDto()
                {
                    AccountId = plr.Account.Id,
                    HP = 350,
                });
            }

            Room.Broadcast(new CaptainRoundCaptainLifeInfoAckMessage(players.ToArray()));
            Rounds++;
        }

        private void RespawnDead(Player plr)
        {
            plr.RoomInfo.State = PlayerState.Alive;
        }


        public override void Update(TimeSpan delta)
        {
            base.Update(delta);
            _nextRoundTimer += delta;

            var teamMgr = Room.TeamManager;
            if (_firstround)
            {
                NextRound();
                _firstround = false;
            }


            if (StateMachine.IsInState(GameRuleState.Playing) &&
                !StateMachine.IsInState(GameRuleState.EnteringResult) &&
                !StateMachine.IsInState(GameRuleState.Result) &&
                RoundTime >= TimeSpan.FromSeconds(5))
            {
                if (_waitingNextRound)
                {
                    if (_nextRoundTimer >= s_nextRoundWaitTime)
                    {
                        StartRound();
                        _waitingNextRound = false;
                        _nextRoundTimer = TimeSpan.Zero;
                    }
                    //else
                    //{
                    //    Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.NextRoundIn, (ulong)s_nextRoundWaitTime.TotalMilliseconds, 0, 0, ""));
                    //}
                }
                else
                {
                    if (Rounds == 10)
                    {
                        StateMachine.Fire(GameRuleStateTrigger.StartResult);
                        return;
                    }
                    if (AlphaPoints >= Room.Options.ScoreLimit || BetaPoints >= Room.Options.ScoreLimit)
                    {
                        StateMachine.Fire(GameRuleStateTrigger.StartResult);
                        return;
                    }
                    AreCaptainsAlive();
                }
            }
        }

        public override PlayerRecord GetPlayerRecord(Player plr)
        {
            return new CaptainPlayerRecord(plr);
        }

        private bool AreCaptainsAlive(Player target = null)
        {
            if (_waitingNextRound)
                return true;

            if (target != null)
            {
                if (target.RoomInfo.Team.Team == Team.Alpha && AlphaCaptains.Contains(target))
                    AlphaCaptains.Remove(target);

                if (target.RoomInfo.Team.Team == Team.Beta && BetaCaptains.Contains(target))
                    BetaCaptains.Remove(target);


                Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.ChatMessage, target.Account.Id, 0, 0, $"[DEBUG] Player {target.Account.Nickname} died"));
               // target.RoomInfo.State = PlayerState.Alive;
            }

            return CaptainWinCheck();
            //if (!AlphaCaptains.Any())
            //{
            //    BetaPoints++;
            //    Room.Broadcast(new CaptainSubRoundWinAckMessage((int)Team.Beta, true));
            //    NextRound();
            //    return false;
            //}

            //if (!BetaCaptains.Any())
            //{
            //    AlphaPoints++;
            //    Room.Broadcast(new CaptainSubRoundWinAckMessage((int)Team.Alpha, true));
            //    NextRound();
            //    return false;
            //}
            //return true;
        }

        private bool CaptainWinCheck()
        {
            if (!AlphaCaptains.Any())
            {
                BetaPoints++;
                Room.Broadcast(new CaptainSubRoundWinAckMessage((int)Team.Beta, true));
                NextRound();
                return false;
            }

            if (!BetaCaptains.Any())
            {
                AlphaPoints++;
                Room.Broadcast(new CaptainSubRoundWinAckMessage((int)Team.Alpha, true));
                NextRound();
                return false;
            }
            return true;
        }

        public override void Respawn(Player victim)
        {
            base.Respawn(victim);
            AreCaptainsAlive(victim);
        }

        public override void OnScoreHeal(Player plr, LongPeerId scoreTarget = null)
        {
            GetRecord(plr).HealAssists++;
            base.OnScoreHeal(plr);
        }

        private bool CanStartGame()
        {
            if (!StateMachine.IsInState(GameRuleState.Waiting))
                return false;

            var teams = Room.TeamManager.Values.ToArray();
            if (Room.Options.IsFriendly)
                return true;
            if (teams.Any(team => team.Count == 0)) // Do we have enough players?
                return false;

            // Is atleast one player per team ready?
            return teams.All(team => team.Players.Any(plr => plr.RoomInfo.IsReady || Room.Master == plr));
        }

        private static DeathmatchPlayerRecord GetRecord(Player plr)
        {
            return (DeathmatchPlayerRecord)plr.RoomInfo.Stats;
        }
    }

    internal class CaptainBriefing : Briefing
    {
        public CaptainBriefing(GameRuleBase RuleBase)
            : base(RuleBase)
        {
        }
    }

    internal class CaptainPlayerRecord : PlayerRecord
    {
        public override uint TotalScore => GetTotalScore();

        public int KillPoints { get; set; }
        public int KillPointsAssists { get; set; }
        public int HealPointsAssists { get; set; }
        public int Unk { get; set; }
        public int Unk2 { get; set; }
        public int Unk3 { get; set; }
        public int Unk4 { get; set; }
        public int CaptainKills { get; set; }
        public int Dominaion { get; set; }
        public int Death { get; set; }

        public CaptainPlayerRecord(Player plr)
            : base(plr)
        { }

        public override void Serialize(BinaryWriter w, bool isResult)
        {
            base.Serialize(w, isResult);

            w.Write(KillPoints);
            w.Write(KillPointsAssists);
            w.Write(HealPointsAssists);
            w.Write(Unk);
            w.Write(Unk2);
            w.Write(Unk3);
            w.Write(Unk4);
            w.Write(CaptainKills);
            w.Write(Dominaion);
            w.Write(Death);

        }

        private uint GetTotalScore()
        {
            return (uint)(KillPoints * 2 + KillPointsAssists
                + CaptainKills * 5 + HealPointsAssists);
        }

        public override void Reset()
        {
            base.Reset();

            KillPoints = 10;
            KillPointsAssists = 5;
            HealPointsAssists = 1;
            Unk = 0;
            Unk2 = 0;
            Unk3 = 0;
            Unk4 = 0;
            CaptainKills = 2;
            Dominaion = 0;
            Death = 0;
        }

        public override uint GetExpGain(out uint bonusExp)
        {
            base.GetExpGain(out bonusExp);

            var config = Config.Instance.Game.TouchdownExpRates;
            var place = 1;

            var plrs = Player.Room.TeamManager.Players
                .Where(plr => plr.RoomInfo.State == PlayerState.Waiting &&
                              plr.RoomInfo.Mode == PlayerGameMode.Normal)
                .ToArray();

            foreach (var plr in plrs.OrderByDescending(plr => plr.RoomInfo.Stats.TotalScore))
            {
                if (plr == Player)
                    break;

                place++;
                if (place > 3)
                    break;
            }

            var rankingBonus = 0f;
            switch (place)
            {
                case 1:
                    rankingBonus = config.FirstPlaceBonus;
                    break;

                case 2:
                    rankingBonus = config.SecondPlaceBonus;
                    break;

                case 3:
                    rankingBonus = config.ThirdPlaceBonus;
                    break;
            }
            var newgain = (TotalScore * config.ScoreFactor +
                           rankingBonus +
                           plrs.Length * config.PlayerCountFactor +
                           Player.RoomInfo.PlayTime.TotalMinutes * config.ExpPerMin);

            return (uint)newgain > 5000 ? 5000 : (uint)newgain;
        }
    }
}


    //internal class CaptainGameRule : GameRuleBase
    //{
    //    public override bool CountMatch => true;
    //    private static readonly TimeSpan s_captainNextroundTime = TimeSpan.FromSeconds(12);
    //    private static readonly TimeSpan s_captainRoundTime = TimeSpan.FromMinutes(5);
    //    private readonly CaptainHelper _captainHelper;
    //    private uint _currentRound;
    //    private TimeSpan _nextRoundTime = TimeSpan.Zero;
    //    private TimeSpan _subRoundTime = TimeSpan.Zero;
    //    private bool _waitingNextRoom;

    //    public override GameRule GameRule => GameRule.Captain;
    //    public override Briefing Briefing { get; }

    //    public CaptainGameRule(Room room)
    //        : base(room)
    //    {
    //        Briefing = new CaptainBriefing(this);
    //        _captainHelper = new CaptainHelper(room);

    //        StateMachine.Configure(GameRuleState.Waiting)
    //            .PermitIf(GameRuleStateTrigger.StartPrepare, GameRuleState.Preparing, CanStartGame);

    //        StateMachine.Configure(GameRuleState.Preparing)
    //            .Permit(GameRuleStateTrigger.StartGame, GameRuleState.FullGame);

    //        StateMachine.Configure(GameRuleState.FullGame)
    //            .SubstateOf(GameRuleState.Playing)
    //            .Permit(GameRuleStateTrigger.StartResult, GameRuleState.EnteringResult);

    //        StateMachine.Configure(GameRuleState.EnteringResult)
    //            .SubstateOf(GameRuleState.Playing)
    //            .Permit(GameRuleStateTrigger.StartResult, GameRuleState.Result);

    //        StateMachine.Configure(GameRuleState.Result)
    //            .SubstateOf(GameRuleState.Playing)
    //            .Permit(GameRuleStateTrigger.EndGame, GameRuleState.Waiting);

    //        //StateMachine.Configure(GameRuleState.Waiting)
    //        //    .PermitIf(GameRuleStateTrigger.StartGame, GameRuleState.FullGame, CanStartGame);

    //        //StateMachine.Configure(GameRuleState.FullGame)
    //        //    .SubstateOf(GameRuleState.Playing)
    //        //    .Permit(GameRuleStateTrigger.StartResult, GameRuleState.EnteringResult)
    //        //    .OnEntry(_captainHelper.Reset);

    //        //StateMachine.Configure(GameRuleState.EnteringResult)
    //        //    .SubstateOf(GameRuleState.Playing)
    //        //    .Permit(GameRuleStateTrigger.StartResult, GameRuleState.Result);

    //        //StateMachine.Configure(GameRuleState.Result)
    //        //    .SubstateOf(GameRuleState.Playing)
    //        //    .Permit(GameRuleStateTrigger.EndGame, GameRuleState.Waiting)
    //        //    .OnEntry(UpdatePlayerStats);
    //    }

    //    public override void Initialize()
    //    {

    //        //var teamMgr = Room.TeamManager;
    //        //teamMgr.Add(Team.Alpha, (uint)(Room.Options.PlayerLimit / 2), (uint)(Room.Options.Spectator / 2));
    //        //teamMgr.Add(Team.Beta, (uint)(Room.Options.PlayerLimit / 2), (uint)(Room.Options.Spectator / 2));
    //        //_currentRound = 0;
    //        //base.Initialize();
    //        var teamMgr = Room.TeamManager;
    //        teamMgr.Add(Team.Alpha, (uint)(Room.Options.PlayerLimit / 2), (uint)(Room.Options.Spectator / 2));
    //        teamMgr.Add(Team.Beta, (uint)(Room.Options.PlayerLimit / 2), (uint)(Room.Options.Spectator / 2));

    //        base.Initialize();
    //    }

    //    public override void Update(TimeSpan delta)
    //    {
    //        base.Update(delta);

    //        //var teamMgr = Room.TeamManager;

    //        //if (StateMachine.IsInState(GameRuleState.Playing) &&
    //        //    !StateMachine.IsInState(GameRuleState.EnteringResult) &&
    //        //    !StateMachine.IsInState(GameRuleState.Result) &&
    //        //    RoundTime >= TimeSpan.FromSeconds(5)) // Let the round run for at least 5 seconds - Fixes StartResult trigger on game start(race condition)
    //        //{
    //        //    // Still have enough players?
    //        //    var min = teamMgr.Values.Min(team =>
    //        //    team.Values.Count(plr =>
    //        //        plr.RoomInfo.State != PlayerState.Lobby &&
    //        //        plr.RoomInfo.State != PlayerState.Spectating));
    //        //    if (min == 0)
    //        //        StateMachine.Fire(GameRuleStateTrigger.StartResult);
    //        var teamMgr = Room.TeamManager;

    //        if (StateMachine.IsInState(GameRuleState.Playing) &&
    //            !StateMachine.IsInState(GameRuleState.EnteringResult) &&
    //            !StateMachine.IsInState(GameRuleState.Result) &&
    //            RoundTime >= TimeSpan.FromSeconds(5))
    //        {
    //            // Still have enough players?
    //            var min = teamMgr.Values.Min(team =>
    //                team.Values.Count(plr =>
    //                    plr.RoomInfo.State != PlayerState.Lobby &&
    //                    plr.RoomInfo.State != PlayerState.Spectating));
    //            if (min == 0 && !Room.Options.IsFriendly)
    //                StateMachine.Fire(GameRuleStateTrigger.StartResult);

    //            var isFirstHalf = StateMachine.IsInState(GameRuleState.FullGame);
    //            if (isFirstHalf)
    //            {
    //                // Did we reach ScoreLimit?
    //                if (teamMgr.Values.Any(team => team.Score >= Room.Options.ScoreLimit))
    //                    StateMachine.Fire(GameRuleStateTrigger.StartResult);

    //                // Did we reach round limit?
    //                if (_currentRound >= Room.Options.TimeLimit.Minutes)
    //                    StateMachine.Fire(GameRuleStateTrigger.StartResult);

    //                _captainHelper.Update(delta);

    //                if (_captainHelper.Any())
    //                    SubRoundEnd();
    //            }

    //            if (_waitingNextRoom)
    //            {
    //                _nextRoundTime += delta;
    //                if (_nextRoundTime >= s_captainNextroundTime)
    //                {
    //                    _captainHelper.Reset();
    //                    _waitingNextRoom = false;
    //                }
    //            }
    //            else
    //            {
    //                _subRoundTime += delta;
    //                if (_subRoundTime >= s_captainRoundTime)
    //                    SubRoundEnd();
    //            }
    //        }
    //    }

    //    public override void Cleanup()
    //    {
    //        var teamMgr = Room.TeamManager;
    //        teamMgr.Remove(Team.Alpha);
    //        teamMgr.Remove(Team.Beta);
    //        base.Cleanup();
    //    }

    //    //public override void PlayerLeft(object room, RoomPlayerEventArgs e)
    //    //{
    //    //    if (StateMachine.IsInState(GameRuleState.FirstHalf))
    //    //        e.Player.CaptainMode.Loss++;

    //    //    base.PlayerLeft(room, e);
    //    //}

    //    public override void OnScoreHeal(Player plr, LongPeerId scoreTarget = null)
    //    {         
    //        GetRecord(plr).Heal++;
    //        base.OnScoreHeal(plr, scoreTarget);
    //    }

    //    public override PlayerRecord GetPlayerRecord(Player plr)
    //    {
    //        return new CaptainPlayerRecord(plr);
    //    }

    //    public override void OnScoreTeamKill(Player killer, Player target, AttackAttribute attackAttribute,
    //        LongPeerId scoreKiller = null, LongPeerId scoreTarget = null)
    //    {
    //        if (_captainHelper.Dead(target) && _captainHelper.Any())
    //            SubRoundEnd();

    //        GetRecord(target).Deaths++;

    //        base.OnScoreTeamKill(killer, target, attackAttribute);
    //    }

    //    public override void OnScoreKill(Player killer, Player assist, Player target, AttackAttribute attackAttribute,
    //        LongPeerId scoreTarget = null, LongPeerId scoreKiller = null, LongPeerId scoreAssist = null)
    //    {
    //        if (_captainHelper.Dead(target))
    //        {
    //            if (_captainHelper.Any())
    //                SubRoundEnd();

    //            GetRecord(killer).KillCaptains++;
    //           // killer.CaptainMode.CPTKilled++;
    //            if (assist != null)
    //                GetRecord(assist).KillAssistCaptains++;
    //        }
    //        else
    //        {
    //            GetRecord(killer).Kills++;
    //            if (assist != null)
    //                GetRecord(assist).KillAssists++;
    //        }

    //        GetRecord(target).Deaths++;

    //        base.OnScoreKill(killer, null, target, attackAttribute);
    //    }

    //    public override void OnScoreSuicide(Player plr, LongPeerId scoreTarget = null)
    //    {
    //        if (_captainHelper.Dead(plr) && _captainHelper.Any())
    //            SubRoundEnd();

    //        GetPlayerRecord(plr).Suicides++;

    //        base.OnScoreSuicide(plr);
    //    }

    //    private bool CanStartGame()
    //    {
    //        if (!StateMachine.IsInState(GameRuleState.Waiting))
    //            return false;

    //        var teams = Room.TeamManager.Values.ToArray();
    //        if (Room.Options.IsFriendly)
    //            return true;
    //        if (teams.Any(team => team.Count == 0)) // Do we have enough players?
    //            return false;

    //        // Is atleast one player per team ready?
    //        return teams.All(team => team.Players.Any(plr => plr.RoomInfo.IsReady || Room.Master == plr));
    //    }

    //    private void SubRoundEnd()
    //    {
    //        var teamwin = _captainHelper.TeamWin();
    //        _currentRound++;

    //        var teamMgr = Room.TeamManager;

    //        // Did we reach ScoreLimit or Round Limit?
    //        if (_currentRound >= Room.Options.TimeLimit.Minutes
    //            || teamMgr.Values.Any(team => team.Score >= Room.Options.ScoreLimit))
    //        {
    //            StateMachine.Fire(GameRuleStateTrigger.StartResult);
    //        }
    //        else
    //        {

    //           // if (!AlphaCaptains.Any())
    //                //            {
    //                //                BetaPoints++;
    //                //                Room.Broadcast(new CaptainSubRoundWinAckMessage((int)Team.Beta, true));
    //                //                NextRound();
    //                //                return false;
    //                //            }

    //                //            if (!BetaCaptains.Any())
    //                //            {
    //                //                AlphaPoints++;
    //                //                Room.Broadcast(new CaptainSubRoundWinAckMessage((int)Team.Alpha, true));
    //                //                NextRound();
    //                //                return false;
    //                //            }
    //                Room.Broadcast(
    //                new CaptainSubRoundWinAckMessage
    //                {
    //                    Unk1 = 0,
    //                    Unk2 = (byte)(teamwin.Team == Team.Alpha ? 1 : 2)
    //                });
    //            Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.NextRoundIn, (ulong)s_captainNextroundTime.TotalMilliseconds, 0, 0, ""));

    //            _nextRoundTime = TimeSpan.Zero;
    //        }

    //        teamwin.Players.First().RoomInfo.Team.Score++;

    //        _subRoundTime = TimeSpan.Zero;
    //    }
    //    //public void NextRound()
    //    //{
    //    //    Room.Broadcast(new CaptainCurrentRoundInfoAckMessage(AlphaPoints, BetaPoints));
    //    //    _nextRoundTime = TimeSpan.Zero;
    //    //   // _waiting = true;
    //    //}

    //    private static CaptainPlayerRecord GetRecord(Player plr)
    //    {
    //        return (CaptainPlayerRecord)plr.RoomInfo.Stats;
    //    }

    //    private void UpdatePlayerStats()
    //    {
    //        var WinTeam = Room
    //            .TeamManager
    //            .PlayersPlaying
    //            .Aggregate(
    //                (highestTeam, player) =>
    //                (highestTeam == null || player.RoomInfo.Team.Score > highestTeam.RoomInfo.Team.Score) ?
    //                player : highestTeam).RoomInfo.Team;

    //        foreach (var plr in Room.TeamManager.PlayersPlaying)
    //        {
    //            //if (plr.RoomInfo.Team == WinTeam)
    //            //  //  plr.CaptainMode.Won++;
    //            //else
    //            //  //  plr.CaptainMode.Loss++;
    //        }
    //    }
    //}

    //internal class CaptainHelper
    //{

    //    public Room Room { get; private set; }

    //    private IEnumerable<Player> _alpha;
    //    private IEnumerable<Player> _beta;
    //    private float _teamLife;

    //    public CaptainHelper(Room room)
    //    {
    //        Room = room;
    //        _alpha = from plr in this.Room.TeamManager.PlayersPlaying
    //                 where plr.RoomInfo.Team.Team == Team.Alpha
    //                 select plr;

    //        _beta = from plr in this.Room.TeamManager.PlayersPlaying
    //                where plr.RoomInfo.Team.Team == Team.Beta
    //                select plr;
    //    }

    //    public void Reset()
    //    {
    //        _alpha = from plr in this.Room.TeamManager.PlayersPlaying
    //                 where plr.RoomInfo.Team.Team == Team.Alpha
    //                 select plr;

    //        _beta = from plr in this.Room.TeamManager.PlayersPlaying
    //                where plr.RoomInfo.Team.Team == Team.Beta
    //                select plr;

    //        float max = (_alpha.Count() > _beta.Count()) ? _alpha.Count() : _beta.Count();

    //        _teamLife = max * 500.0f;

    //        var players = (from plr in this.Room.TeamManager.PlayersPlaying
    //                       select new CaptainLifeDto { AccountId = plr.Account.Id, HP = _teamLife / plr.RoomInfo.Team.Count() })
    //                      .ToArray();

    //        foreach (var plr in Room.TeamManager.PlayersPlaying)
    //        {
    //            plr.RoomInfo.State = PlayerState.Alive;
    //            //plr.CaptainMode.CPTCount++;
    //        }

    //        Room.Broadcast(new CaptainRoundCaptainLifeInfoAckMessage { Players = players });
    //        Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.ResetRound, 0, 0, 0, ""));
    //    }

    //    public bool Dead(Player target)
    //    {
    //        if (target.RoomInfo.Team.Team == Team.Alpha)
    //        {
    //            var isCaptain = (from plr in _alpha
    //                             where plr == target
    //                             select plr).Any();

    //            _alpha = from plr in _alpha
    //                     where plr != target
    //                     select plr;

    //            target.Room.Broadcast(new CaptainCurrentRoundInfoAckMessage { Unk1 = _alpha.Count(), Unk2 = _beta.Count() });

    //            return isCaptain;
    //        }

    //        if (target.RoomInfo.Team.Team == Team.Beta)
    //        {
    //            var isCaptain = (from plr in _beta
    //                             where plr == target
    //                             select plr).Any();

    //            _beta = from plr in _beta
    //                    where plr != target
    //                    select plr;

    //            target.Room.Broadcast(new CaptainCurrentRoundInfoAckMessage { Unk1 = _alpha.Count(), Unk2 = _beta.Count() });

    //            return isCaptain;
    //        }

    //        return false;// we need this?
    //    }

    //    public bool Any()
    //    {
    //        return !_alpha.Any() || !_beta.Any();
    //    }

    //    public PlayerTeam TeamWin()
    //    {
    //        if (!_alpha.Any())
    //            return Room.TeamManager.GetValueOrDefault(Team.Beta);

    //        if (!_beta.Any())
    //            return Room.TeamManager.GetValueOrDefault(Team.Alpha);

    //        return (_alpha.Count() > _beta.Count()) ?
    //            Room.TeamManager.GetValueOrDefault(Team.Alpha) :
    //            Room.TeamManager.GetValueOrDefault(Team.Beta);
    //    }

    //    public void Update(TimeSpan delta)
    //    {
    //        _alpha = from plr in Room.TeamManager.PlayersPlaying
    //                 join oplr in _alpha on plr equals oplr
    //                 select plr;

    //        _beta = from plr in Room.TeamManager.PlayersPlaying
    //                join oplr in _beta on plr equals oplr
    //                select plr;
    //    }
    //}

    //internal class CaptainBriefing : Briefing
    //{
    //    public CaptainBriefing(GameRuleBase RuleBase)
    //        : base(RuleBase)
    //    {
    //    }
    //}

    //internal class CaptainPlayerRecord : PlayerRecord
    //{
    //    public override uint TotalScore => (5 * (WinRound + KillCaptains)) + (2 * Kills) + KillAssists + Heal - Suicides;
    //    public uint KillCaptains { get; set; }
    //    public uint KillAssistCaptains { get; set; }
    //    public uint WinRound { get; set; }
    //    public uint Heal { get; set; }

    //    public CaptainPlayerRecord(Player plr)
    //        : base(plr)
    //    {
    //    }

    //    public override void Serialize(BinaryWriter w, bool isResult)
    //    {
    //        base.Serialize(w, isResult);

    //        w.Write(KillCaptains);
    //        w.Write(KillAssistCaptains);
    //        w.Write(Kills);
    //        w.Write(KillAssists);
    //        w.Write(Heal);
    //        w.Write(WinRound);
    //    }

    //    public override void Reset()
    //    {
    //        base.Reset();
    //        KillCaptains = 0;
    //        KillAssistCaptains = 0;
    //        Heal = 0;
    //    }

    //    public override uint GetExpGain(out uint bonusExp)
    //    {
    //        base.GetExpGain(out bonusExp);

    //        var config = Config.Instance.Game.TouchdownExpRates;
    //        var place = 1;

    //        var plrs = Player.Room.TeamManager.Players
    //            .Where(plr => plr.RoomInfo.State == PlayerState.Waiting &&
    //                          plr.RoomInfo.Mode == PlayerGameMode.Normal)
    //            .ToArray();

    //        foreach (var plr in plrs.OrderByDescending(plr => plr.RoomInfo.Stats.TotalScore))
    //        {
    //            if (plr == Player)
    //                break;

    //            place++;
    //            if (place > 3)
    //                break;
    //        }

    //        var rankingBonus = 0f;
    //        switch (place)
    //        {
    //            case 1:
    //                rankingBonus = config.FirstPlaceBonus;
    //                break;

    //            case 2:
    //                rankingBonus = config.SecondPlaceBonus;
    //                break;

    //            case 3:
    //                rankingBonus = config.ThirdPlaceBonus;
    //                break;
    //        }
    //        var newgain = (TotalScore * config.ScoreFactor +
    //                       rankingBonus +
    //                       plrs.Length * config.PlayerCountFactor +
    //                       Player.RoomInfo.PlayTime.TotalMinutes * config.ExpPerMin);

    //        return (uint)newgain > 5000 ? 5000 : (uint)newgain;
    //    }
    //}}


