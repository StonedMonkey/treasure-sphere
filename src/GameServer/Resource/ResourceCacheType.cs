namespace TreasureSphere.Resource
{
    internal enum ResourceCacheType
    {
        Clubs,
        Channels,
        Effects,
        Items,
        DefaultItems,
        Shop,
        Exp,
        Maps,
        GameTempo
    }
}
