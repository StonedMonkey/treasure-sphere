﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TreasureSphere.Network;

namespace TreasureSphere.Commands
{
    internal class HelpCommand : ICommand
    {
        public HelpCommand()
        {
            Name = "/hp";
            AllowConsole = true;
            Permission = SecurityLevel.GameMaster;
            SubCommands = new ICommand[] { };
        }

        public string Name { get; }
        public bool AllowConsole { get; }
        public SecurityLevel Permission { get; }
        public IReadOnlyList<ICommand> SubCommands { get; }

        public bool Execute(GameServer server, Player plr, string[] args)
        {
            plr.SendConsoleMessage(S4Color.Yellow + ">>>> Game Master Help System <<<<<");
            plr.SendConsoleMessage(S4Color.Yellow + ">>>> For all Commands u need to whisper urself!");
            plr.SendConsoleMessage(S4Color.Green + "> /whole_notice msg (Rank >1<)");
            plr.SendConsoleMessage(S4Color.Green + "> admin rename/playerkick/roomkick (Rank >1<) | level/seclevel/ap (Rank >3<)");
            plr.SendConsoleMessage(S4Color.Green + "> /ban (for more infos just /ban) (Rank >2<)");
            plr.SendConsoleMessage(S4Color.Green + "> game start (Rank >1<) | pause (Rank >2<//KINDABUGGED) | state StartResult/StartHalfTime (Rank >2<)");
            plr.SendConsoleMessage(S4Color.Green + "> gm setmaster | td <player> <a/b> for Team Alpha/Beta");

            return true;
        }

        public string Help()
        {
            var sb = new StringBuilder();
            sb.AppendLine(Name);
            foreach (var cmd in SubCommands)
            {
                sb.Append(" ");
                sb.AppendLine(cmd.Help());
            }
            return sb.ToString();
        }
    }
}

