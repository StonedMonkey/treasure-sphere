﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Dapper.FastCrud;
using TreasureSphere.Database.Auth;
using TreasureSphere.Network;
using TreasureSphere.Network.Message.Chat;
using TreasureSphere.Network.Message.Game;
using TreasureSphere.Network.Message.GameRule;
using System.Timers;
using System.Threading;
using TreasureSphere.Database.Game;

namespace TreasureSphere.Commands
{
    internal class GMCommands : ICommand
    {
        public string Name { get; }
        public bool AllowConsole { get; }
        public RoomManager RoomManager { get; }
        public SecurityLevel Permission { get; }
        public IReadOnlyList<ICommand> SubCommands { get; }

        public GMCommands()
        {
            Name = "gm";
            AllowConsole = true;
            Permission = SecurityLevel.GameMaster;
            SubCommands = new ICommand[] { new KillRoomCommand(), new GetIDCommand(), new SetMasterCommand(), new SetApCommand(), new SetPenCommand(), new TD() };
        }

        public bool Execute(GameServer server, Player plr, string[] args)
        {
            return true;
        }

        public string Help()
        {
            var sb = new StringBuilder();
            sb.AppendLine(Name);
            foreach (var cmd in SubCommands)
            {
                sb.Append("\t");
                sb.AppendLine(cmd.Help());
            }
            return sb.ToString();
        }

        private class SetApCommand : ICommand
        {
            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public SetApCommand()
            {
                Name = "setap";
                AllowConsole = false;
                Permission = SecurityLevel.Administrator;
                SubCommands = new ICommand[0];
            }

            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (args.Length < 1)
                    return false;

                if (!uint.TryParse(args[0], out uint val))
                    return false;

                plr.AP = val;
                plr.NeedsToSave = true;
                plr.Session.SendAsync(new MoneyRefreshCashInfoAckMessage(plr.PEN, plr.AP));
                return true;
            }

            public string Help()
            {
                return Name;
            }
        }

        private class SetPenCommand : ICommand
        {
            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public SetPenCommand()
            {
                Name = "setpen";
                AllowConsole = false;
                Permission = SecurityLevel.Administrator;
                SubCommands = new ICommand[0];
            }

            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (args.Length < 1)
                    return false;

                if (!uint.TryParse(args[0], out uint val))
                    return false;

                plr.PEN = val;
                plr.NeedsToSave = true;
                plr.Session.SendAsync(new MoneyRefreshCashInfoAckMessage(plr.PEN, plr.AP));
                return true;
            }

            public string Help()
            {
                return Name;
            }
        }

        private class SetMasterCommand : ICommand
        {
            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public SetMasterCommand()
            {
                Name = "setmaster";
                AllowConsole = false;
                Permission = SecurityLevel.GameMaster;
                SubCommands = new ICommand[0];
            }

            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (args.Length > 0)
                {
                    Player searchplr = GameServer.Instance.PlayerManager.Get(args[0]);
                    if (searchplr != null)
                    {
                        if (searchplr.Room != null)
                        {
                            searchplr.Room.ChangeMasterIfNeeded(searchplr, true);
                            searchplr.Room.ChangeHostIfNeeded(searchplr, true);
                        }
                        else
                            plr.SendConsoleMessage(S4Color.Red + "Player is not in a room!");
                    }
                }
                else
                {
                    if (plr.Room == null)
                        plr.SendConsoleMessage(S4Color.Red + "You are not in a room!");
                    else
                    {
                        plr.Room.ChangeMasterIfNeeded(plr, true);
                        plr.Room.ChangeHostIfNeeded(plr, true);
                    }
                }

                return true;
            }

            public string Help()
            {
                return Name;
            }
        }

        private class GetIDCommand : ICommand
        {
            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public GetIDCommand()
            {
                Name = "getid";
                AllowConsole = true;
                Permission = SecurityLevel.GameMaster;
                SubCommands = new ICommand[0];
            }

            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (args.Length < 1)
                    return false;
                Player searchplr = GameServer.Instance.PlayerManager.Get(args[0]);
                if (searchplr != null)
                {
                    if (plr != null)
                        plr.SendConsoleMessage(S4Color.Green + $"> {searchplr.Account.Nickname}'s id is {searchplr.Account.Id}");
                    else
                        Console.WriteLine($"> {searchplr.Account.Nickname}'s id is {searchplr.Account.Id}");

                    return true;
                }
                else
                    plr.SendConsoleMessage(S4Color.Red + "Unknown player!");

                return false;
            }
            
            public string Help()
            {
                return Name;
            }
        }

        private class KillRoomCommand : ICommand
        {
            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public KillRoomCommand()
            {
                Name = "killroom";
                AllowConsole = true;
                Permission = SecurityLevel.Administrator;
                SubCommands = new ICommand[0];
            }
            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (args.Length < 2)
                    return false;
                uint channelId;
                uint roomId;
                if (!uint.TryParse(args[0], out channelId))
                    return false;
                if (!uint.TryParse(args[1], out roomId))
                    return false;
                Room room = server.ChannelManager.GetChannel(channelId).RoomManager.Get(roomId);

                if (room.Players.Count > 0)
                {
                    foreach (var kplr in room.Players.Values)
                        kplr.Room.Leave(kplr, RoomLeaveReason.ModeratorKick);
                }
                else
                {
                    room.RoomManager.Remove(room);
                }
                //todo add chat message to server explaining room was deleted
                return true;
            }
            public string Help()
            {
                return Name;
            }
        }
        private class TD : ICommand
        {
            public TD()
            {
                Name = "td";
                AllowConsole = false;
                Permission = SecurityLevel.GameMaster;
                SubCommands = new ICommand[0];
            }
            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }


            private static readonly TimeSpan TouchdownWaitTime = TimeSpan.FromSeconds(12);
            private TimeSpan _touchdownTime = TimeSpan.FromSeconds(0);
            //Timer restart = new Timer();
            public bool Execute(GameServer server, Player plr, string[] args)
            {              
                if (args.Length <= 1)
                {
                    plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                    plr.SendConsoleMessage(S4Color.Red + "> td <username> <a/b>");
                    return true;
                }

                if (args.Length >= 2)
                {
                    AccountDto account;
                    PlayerDto playerdto;
                    using (var db = AuthDatabase.Open())
                    {
                        account = (db.Find<AccountDto>(statement => statement
                               .Include<BanDto>(join => join.LeftOuterJoin())
                               .Where($"{nameof(AccountDto.Nickname):C} = @Nickname")
                               .WithParameters(new { Nickname = args[0] })))
                            .FirstOrDefault();

                        if (account == null)
                        {
                            plr.SendConsoleMessage(S4Color.Red + "Unknown player!");
                            return true;
                        }

                        playerdto = (db.Find<PlayerDto>(statement => statement
                               .Where($"{nameof(PlayerDto.Id):C} = @Id")
                               .WithParameters(new { account.Id })))
                            .FirstOrDefault();

                        if (playerdto == null)
                        {
                            plr.SendConsoleMessage(S4Color.Red + "Unknown player!");
                            return true;
                        }
                        else
                        {

                            plr.SendConsoleMessage(S4Color.Green + $"Set TD for {account.Nickname}");


                            var player = GameServer.Instance.PlayerManager.Get((ulong)account.Id);
                            if (player != null && player.Session.Player.Room != null && args[1] == "a")
                            {

                                plr.Room.Broadcast(new ScoreGoalAckMessage(player.Account.Id));
                                plr.Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.TouchdownAlpha, 0, 0, 0, ""));
                                plr.Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.NextRoundIn, (ulong)TouchdownWaitTime.TotalMilliseconds, 0, 0, ""));
                                plr.Room.Broadcast(new NoticeAdminMessageAckMessage($"{plr.Account.Nickname} gave a TD to {player.Account.Nickname}"));
                                var delay = Task.Delay(12000).ContinueWith(_ => plr.Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.ResetRound, (ulong)TouchdownWaitTime.TotalMilliseconds, 0, 0, "")));
                            }
                            else if(player != null && player.Session.Player.Room != null && args[1] == "b")
                            {
                                plr.Room.Broadcast(new ScoreGoalAckMessage(player.Account.Id));
                                plr.Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.TouchdownBeta, 0, 0, 0, ""));
                                plr.Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.NextRoundIn, (ulong)TouchdownWaitTime.TotalMilliseconds, 0, 0, ""));
                                plr.Room.Broadcast(new NoticeAdminMessageAckMessage($"{plr.Account.Nickname} gave a TD to {player.Account.Nickname}"));
                                var delay = Task.Delay(12000).ContinueWith(_ => plr.Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.ResetRound, (ulong)TouchdownWaitTime.TotalMilliseconds, 0, 0, "")));
                            }
                            else
                            {
                                plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                                plr.SendConsoleMessage(S4Color.Red + "> td <username> <a/b>");
                            }

                        }
                    }
                }

                return true;
            }
            public string Help()
            {
                return Name;
            }
        }
    }
}
