﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TreasureSphere.Network;
using TreasureSphere.Network.Message.Game;

namespace TreasureSphere.Commands
{
    internal class RNoticeCommand : ICommand
    {
        public RNoticeCommand()
        {
            Name = "/room_notice";
            AllowConsole = true;
            Permission = SecurityLevel.GameMaster;
            SubCommands = new ICommand[] { };
        }

        public string Name { get; }
        public bool AllowConsole { get; }
        public SecurityLevel Permission { get; }
        public IReadOnlyList<ICommand> SubCommands { get; }

        public bool Execute(GameServer server, Player plr, string[] args)
        {
            var notice = new StringBuilder();
            foreach (string x in args.ToList())
                notice.Append(" " + x);
            plr.Room.Broadcast(new NoticeAdminMessageAckMessage(plr.Account.Nickname+ ": " + notice.ToString().Replace("/room_notice","")));

            return true;
        }

        public string Help()
        {
            var sb = new StringBuilder();
            sb.AppendLine(Name);
            foreach (var cmd in SubCommands)
            {
                sb.Append(" ");
                sb.AppendLine(cmd.Help());
            }
            return sb.ToString();
        }
    }
}
