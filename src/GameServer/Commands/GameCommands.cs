﻿using System;
using System.Collections.Generic;
using System.Text;
using TreasureSphere.Network;
using TreasureSphere.Network.Message.Game;
using TreasureSphere;
using TreasureSphere.Network.Message.GameRule;
using System.Threading.Tasks;

namespace TreasureSphere.Commands
{
    internal class GameCommands : ICommand
    {
        public GameCommands()
        {
            Name = "game";
            AllowConsole = false;
            Permission = SecurityLevel.Developer;
            SubCommands = new ICommand[] { new StateCommand(), new StartCommand(),new PauseCommand() };
        }

        public string Name { get; }
        public bool AllowConsole { get; }
        public SecurityLevel Permission { get; }
        public IReadOnlyList<ICommand> SubCommands { get; }

        public bool Execute(GameServer server, Player plr, string[] args)
        {
            return true;
        }

        public string Help()
        {
            var sb = new StringBuilder();
            sb.AppendLine(Name);
            foreach (var cmd in SubCommands)
            {
                sb.Append(" ");
                sb.AppendLine(cmd.Help());
            }
            return sb.ToString();
        }

        private class StateCommand : ICommand
        {
            public StateCommand()
            {
                Name = "state";
                AllowConsole = false;
                Permission = SecurityLevel.Developer;
                SubCommands = new ICommand[0];
            }

            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (plr.Room == null)
                {
                    plr.SendConsoleMessage(S4Color.Red + "You're not inside a room");
                    return false;
                }
                var stateMachine = plr.Room.GameRuleManager.GameRule.StateMachine;
                if (args.Length == 0)
                {
                    plr.SendConsoleMessage($"Current state: {stateMachine.State}");
                }
                else
                {
                    GameRuleStateTrigger trigger;
                    if (!Enum.TryParse(args[0], out trigger))
                    {
                        plr.SendConsoleMessage(
                            $"{S4Color.Red}Invalid trigger! Available triggers: {string.Join(",", stateMachine.PermittedTriggers)}");
                    }
                    else
                    {
                        stateMachine.Fire(trigger);
                        plr.Room.Broadcast(
                            new NoticeAdminMessageAckMessage(
                                $"Current game state has been changed by {plr.Account.Nickname}"));
                    }
                }

                return true;
            }

            public string Help()
            {
                return Name + " [trigger]";
            }
        }
        private class StartCommand : ICommand
        {
            public StartCommand()
            {
                Name = "start";
                AllowConsole = false;
                Permission = SecurityLevel.GameMaster;
                SubCommands = new ICommand[0];
            }
            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            private static readonly TimeSpan TouchdownWaitTime = TimeSpan.FromSeconds(8);
            public bool Execute(GameServer server, Player plr, string[] args)
            {

                if (plr.Room == null)
                {
                    plr.SendConsoleMessage(S4Color.Red + "You're not inside a Room!");
                    return false;
                }
                if (args.Length == 0)
                {
                    //  plr.Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.ResetRound, 0, 0, 0, ""));
                    plr.Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.NextRoundIn, (ulong)TouchdownWaitTime.TotalMilliseconds, 0, 0, ""));
                    plr.Room.Broadcast(new NoticeAdminMessageAckMessage($"{plr.Account.Nickname} restarted the Round"));
                   // plr.Room.Broadcast(new NoticeAdminMessageAckMessage($"{plr.Account.Nickname} gave a TD to {plr.Account.Nickname}"));
                    var delay = Task.Delay(8000).ContinueWith(_ => plr.Room.Broadcast(new GameEventMessageAckMessage(GameEventMessage.ResetRound, (ulong)TouchdownWaitTime.TotalMilliseconds, 0, 0, "")));

                }
                else
                {
                    plr.SendConsoleMessage(S4Color.Red + "Error");
                    return false;
                }

                return true;
            }
            public string Help()
            {
                return Name;
            }
        }
        private class PauseCommand : ICommand
        {
            public PauseCommand()
            {
                Name = "pause";
                AllowConsole = false;
                Permission = SecurityLevel.Developer;
                SubCommands = new ICommand[0];
            }
            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (plr.Room == null)
                {
                    plr.SendConsoleMessage(S4Color.Red + "You're not inside a Room!");
                    return false;
                }
                if (args.Length == 0)
                {
                    plr.Room.Broadcast(new ScoreGoalAckMessage(plr.RoomInfo.PeerId));
                    plr.Room.Broadcast(new NoticeAdminMessageAckMessage($"{plr.Account.Nickname} paused the Round"));
                }
                else
                {
                    plr.SendConsoleMessage(S4Color.Red + "Error");
                    return false;
                }

                return true;
            }
            public string Help()
            {
                return Name;
            }
        }
    }
}
