﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Dapper.FastCrud;
using ExpressMapper.Extensions;
using TreasureSphere.Database.Auth;
using TreasureSphere.Database.Game;
using TreasureSphere.Network;
using TreasureSphere.Network.Data.Game;
using TreasureSphere.Network.Message.Game;
using TreasureSphere.Resource;

namespace TreasureSphere.Commands
{
    internal class AdminCommands : ICommand
    {
        public AdminCommands()
        {
            AccountDto account;
            Name = "admin";
            AllowConsole = true;
            Permission = SecurityLevel.GameMaster;
            SubCommands = new ICommand[] { new RenameCommand(), new SecurityCommand(), new LevelCommand(), new PlayerKickCommand(), new APCommand(), new RoomKickCommand() };
        }

        public string Name { get; }
        public bool AllowConsole { get; }
        public SecurityLevel Permission { get; }
        public IReadOnlyList<ICommand> SubCommands { get; }

        public bool Execute(GameServer server, Player plr, string[] args)
        {
            return true;
        }

        public string Help()
        {
            var sb = new StringBuilder();
            sb.AppendLine(Name);
            foreach (var cmd in SubCommands)
            {
                sb.Append(" ");
                sb.AppendLine(cmd.Help());
            }
            return sb.ToString();
        }

        private class RenameCommand : ICommand
        {
            public RenameCommand()
            {
                Name = "rename";
                AllowConsole = true;
                Permission = SecurityLevel.GameMaster;
                SubCommands = new ICommand[] { };
            }

            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (args.Length < 1)
                {
                    plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                    plr.SendConsoleMessage(S4Color.Red + "> rename <username> <newname>");
                    return true;
                }

                if(args.Length >= 2)
                {
                    AccountDto account;
                    using (var db = AuthDatabase.Open())
                    {
                        account = (db.Find<AccountDto>(statement => statement
                               .Include<BanDto>(join => join.LeftOuterJoin())
                               .Where($"{nameof(AccountDto.Nickname):C} = @Nickname")
                               .WithParameters(new { Nickname = args[0] })))
                            .FirstOrDefault();

                        if (account == null)
                        {
                            plr.SendConsoleMessage(S4Color.Red + "Unknown player!");
                            return true;
                        }
                        else
                        {
                            plr.SendConsoleMessage(S4Color.Green + $"Changed {account.Nickname}'s nickname to {args[1]}");
                            account.Nickname = args[1];
                            db.Update(account);

                            var player = GameServer.Instance.PlayerManager.Get((ulong)account.Id);
                            player?.Session?.SendAsync(new ItemUseChangeNickAckMessage() { Result = 0 });
                            player?.Session?.SendAsync(new ServerResultAckMessage(ServerResult.CreateNicknameSuccess));
                        }
                    }
                }
                
                return true;
            }

            public string Help()
            {
                return Name;
            }
        }

        private class SecurityCommand : ICommand
        {
            public SecurityCommand()
            {
                Name = "seclevel";
                AllowConsole = true;
                Permission = SecurityLevel.Administrator;
                SubCommands = new ICommand[] { };
            }

            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (args.Length < 1)
                {
                    plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                    plr.SendConsoleMessage(S4Color.Red + "> seclevel <username> <level>");
                    return true;
                }

                if (args.Length >= 2)
                {
                    AccountDto account;
                    using (var db = AuthDatabase.Open())
                    {
                        account = (db.Find<AccountDto>(statement => statement
                               .Include<BanDto>(join => join.LeftOuterJoin())
                               .Where($"{nameof(AccountDto.Nickname):C} = @Nickname")
                               .WithParameters(new { Nickname = args[0] })))
                            .FirstOrDefault();

                        if (account == null)
                        {
                            plr.SendConsoleMessage(S4Color.Red + "Unknown player!");
                            return true;
                        }
                        else
                        {
                            if (byte.TryParse(args[1], out var level))
                            {
                                plr.SendConsoleMessage(S4Color.Green + $"Changed {account.Nickname}'s seclevel to {args[1]}");
                                account.SecurityLevel = level;
                                db.Update(account);

                                var player = GameServer.Instance.PlayerManager.Get((ulong)account.Id);
                                player?.Session?.SendAsync(new ItemUseChangeNickAckMessage() { Result = 0 });
                                player?.Session?.SendAsync(new ServerResultAckMessage(ServerResult.CreateNicknameSuccess));
                            }
                            else
                            {
                                plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                                plr.SendConsoleMessage(S4Color.Red + "> seclevel <username> <level>");
                                plr.SendConsoleMessage(S4Color.Red + "> seclevel <level>");
                            }

                        }
                    }
                }

                return true;
            }

            public string Help()
            {
                return Name;
            }
        }

        private class LevelCommand : ICommand
        {
            public LevelCommand()
            {
                Name = "level";
                AllowConsole = true;
                Permission = SecurityLevel.Administrator;
                SubCommands = new ICommand[] { };
            }

            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (args.Length < 1)
                {
                    plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                    plr.SendConsoleMessage(S4Color.Red + "> level <username> <level>");
                    return true;
                }

                if (args.Length >= 2)
                {
                    AccountDto account;
                    PlayerDto playerdto;
                    using (var db = AuthDatabase.Open())
                    {
                        account = (db.Find<AccountDto>(statement => statement
                               .Include<BanDto>(join => join.LeftOuterJoin())
                               .Where($"{nameof(AccountDto.Nickname):C} = @Nickname")
                               .WithParameters(new { Nickname = args[0] })))
                            .FirstOrDefault();
                        
                        if (account == null)
                        {
                            plr.SendConsoleMessage(S4Color.Red + "Unknown player!");
                            return true;
                        }

                        playerdto = (db.Find<PlayerDto>(statement => statement
                               .Where($"{nameof(PlayerDto.Id):C} = @Id")
                               .WithParameters(new { account.Id })))
                            .FirstOrDefault();

                        if (playerdto == null)
                        {
                            plr.SendConsoleMessage(S4Color.Red + "Unknown player!");
                            return true;
                        }
                        else
                        {
                            if (byte.TryParse(args[1], out var level))
                            {
                                plr.SendConsoleMessage(S4Color.Green + $"Changed {account.Nickname}'s level to {args[1]}");
                                plr.Level = level;
                                playerdto.Level = level;
                                db.Update(playerdto);

                                var player = GameServer.Instance.PlayerManager.Get((ulong)account.Id);
                                if(player != null)
                                {
                                    if (level > 80)
                                    {
                                        plr.SendConsoleMessage(S4Color.Red + "Level to High");
                                        return true;
                                    }
                                    if (level == 80)
                                    {
                                        var expTable = GameServer.Instance.ResourceCache.GetExperience();
                                        Experience expValue = new Experience();
                                        var exp = expTable.TryGetValue(level, out expValue);

                                        player.Level = level;

                                        player.TotalExperience = expValue.TotalExperience;
                                        player.Session?.SendAsync(new PlayerAccountInfoAckMessage(player.Map<Player, PlayerAccountInfoDto>()));
                                        player.NeedsToSave = true;
                                    }
                                    else
                                    {
                                        var expTable = GameServer.Instance.ResourceCache.GetExperience();
                                        Experience expValue = new Experience();
                                        var exp = expTable.TryGetValue(level, out expValue);

                                        player.Level = level;

                                        player.TotalExperience = expValue.TotalExperience - expValue.ExperienceToNextLevel;
                                        player.Session?.SendAsync(new PlayerAccountInfoAckMessage(player.Map<Player, PlayerAccountInfoDto>()));
                                        player.NeedsToSave = true;
                                    }
                                }
                            }
                            else
                            {
                                plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                                plr.SendConsoleMessage(S4Color.Red + "> level <username> <level>");
                            }

                        }
                    }
                }

                return true;
            }

            public string Help()
            {
                return Name;
            }
        }
        
        private class PlayerKickCommand : ICommand
        {
            public PlayerKickCommand()
            {
                Name = "playerkick";
                AllowConsole = true;
                Permission = SecurityLevel.GameMaster;
                SubCommands = new ICommand[] { };
            }

            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (args.Length < 0)
                {
                    plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                    plr.SendConsoleMessage(S4Color.Red + "> playerkick <username>");
                    return true;
                }

                if (args.Length >= 1)
                {
                    AccountDto account;
                    PlayerDto playerdto;
                    using (var db = AuthDatabase.Open())
                    {
                        account = (db.Find<AccountDto>(statement => statement
                               .Include<BanDto>(join => join.LeftOuterJoin())
                               .Where($"{nameof(AccountDto.Nickname):C} = @Nickname")
                               .WithParameters(new { Nickname = args[0] })))
                            .FirstOrDefault();

                        if (account == null)
                        {
                            plr.SendConsoleMessage(S4Color.Red + "Unknown player!");
                            return true;
                        }

                        playerdto = (db.Find<PlayerDto>(statement => statement
                               .Where($"{nameof(PlayerDto.Id):C} = @Id")
                               .WithParameters(new { account.Id })))
                            .FirstOrDefault();

                        if (playerdto == null)
                        {
                            plr.SendConsoleMessage(S4Color.Red + "Unknown player!");
                            return true;
                        }
                        else
                        {
                            //if (byte.TryParse(args[1], out var level))
                            //{
                                plr.SendConsoleMessage(S4Color.Green + $"Kicked Player :  {account.Nickname}!");
                                // plr.AP += level;
                                //playerdto.AP += level;
                               // db.Update(playerdto);

                                var player = GameServer.Instance.PlayerManager.Get((ulong)account.Id);
                                if (player != null)
                                {
                                    //player.AP += level;
                                  //  player.Session?.SendAsync(new PlayerAccountInfoAckMessage(player.Map<Player, PlayerAccountInfoDto>()));
                                  //  player.NeedsToSave = true;
                                    player?.Session?.CloseAsync();
                                }
                            //}
                            //else
                            //{
                            //    plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                            //    plr.SendConsoleMessage(S4Color.Red + "> player <username>");
                            //}

                        }
                    }
                }

                return true;
            }

            public string Help()
            {
                return Name;
            }
        }
        private class APCommand : ICommand
        {
            public APCommand()
            {
                Name = "giveap";
                AllowConsole = true;
                Permission = SecurityLevel.Developer;
                SubCommands = new ICommand[] { };
            }

            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (args.Length < 1)
                {
                    plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                    plr.SendConsoleMessage(S4Color.Red + "> ap <username> <ap>");
                    return true;
                }

                if (args.Length >= 2)
                {
                    AccountDto account;
                    PlayerDto playerdto;
                    using (var db = AuthDatabase.Open())
                    {
                        account = (db.Find<AccountDto>(statement => statement
                               .Include<BanDto>(join => join.LeftOuterJoin())
                               .Where($"{nameof(AccountDto.Nickname):C} = @Nickname")
                               .WithParameters(new { Nickname = args[0] })))
                            .FirstOrDefault();

                        if (account == null)
                        {
                            plr.SendConsoleMessage(S4Color.Red + "Unknown player!");
                            return true;
                        }

                        playerdto = (db.Find<PlayerDto>(statement => statement
                               .Where($"{nameof(PlayerDto.Id):C} = @Id")
                               .WithParameters(new { account.Id })))
                            .FirstOrDefault();

                        if (playerdto == null)
                        {
                            plr.SendConsoleMessage(S4Color.Red + "Unknown player!");
                            return true;
                        }
                        else
                        {
                            if (byte.TryParse(args[1], out var level))
                            {
                                plr.SendConsoleMessage(S4Color.Green + $"Added {args[1]} AP to {account.Nickname}");
                                // plr.AP += level;
                                playerdto.AP += level;
                                db.Update(playerdto);

                                var player = GameServer.Instance.PlayerManager.Get((ulong)account.Id);
                                if (player != null)
                                {
                                    player.AP += level;
                                    player.Session?.SendAsync(new PlayerAccountInfoAckMessage(player.Map<Player, PlayerAccountInfoDto>()));
                                    player.NeedsToSave = true;
                                   // player?.Session?.SendAsync(new ItemUseChangeNickAckMessage() { Result = 0 });
                                    player?.Session?.SendAsync(new MoneyRefreshCashInfoAckMessage(player.PEN, player.AP));
                                    //player?.Session?.SendAsync(new ServerResultAckMessage(ServerResult.CreateNicknameSuccess));
                                }
                            }
                            else
                            {
                                plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                                plr.SendConsoleMessage(S4Color.Red + "> giveap <username> <ap>");
                            }

                        }
                    }
                }

                return true;
            }

            public string Help()
            {
                return Name;
            }
        }
        private class RoomKickCommand : ICommand
        {
            public RoomKickCommand()
            {
                Name = "roomkick";
                AllowConsole = true;
                Permission = SecurityLevel.GameMaster;
                SubCommands = new ICommand[] { };
            }

            public string Name { get; }
            public bool AllowConsole { get; }
            public SecurityLevel Permission { get; }
            public IReadOnlyList<ICommand> SubCommands { get; }

            public bool Execute(GameServer server, Player plr, string[] args)
            {
                if (args.Length < 0)
                {
                    plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                    plr.SendConsoleMessage(S4Color.Red + "> room <username>");
                    return true;
                }

                if (args.Length >= 1)
                {
                    AccountDto account;
                    PlayerDto playerdto;
                    using (var db = AuthDatabase.Open())
                    {
                        account = (db.Find<AccountDto>(statement => statement
                               .Include<BanDto>(join => join.LeftOuterJoin())
                               .Where($"{nameof(AccountDto.Nickname):C} = @Nickname")
                               .WithParameters(new { Nickname = args[0] })))
                            .FirstOrDefault();

                        if (account == null)
                        {
                            plr.SendConsoleMessage(S4Color.Red + "Unknown player!");
                            return true;
                        }

                        playerdto = (db.Find<PlayerDto>(statement => statement
                               .Where($"{nameof(PlayerDto.Id):C} = @Id")
                               .WithParameters(new { account.Id })))
                            .FirstOrDefault();

                        if (playerdto == null)
                        {
                            plr.SendConsoleMessage(S4Color.Red + "Unknown player!");
                            return true;
                        }
                        else
                        {

                            plr.SendConsoleMessage(S4Color.Green + $"Kicked Player :  {account.Nickname}! our of Room {plr.Room.Id}");


                            var player = GameServer.Instance.PlayerManager.Get((ulong)account.Id);
                            if (player != null && player.Session.Player.Room != null)
                            {     
                                player.Room.Leave(player, RoomLeaveReason.ModeratorKick);
                            }
                            else
                            {
                            plr.SendConsoleMessage(S4Color.Red + "Wrong Usage, possible usages:");
                            plr.SendConsoleMessage(S4Color.Red + "> roomkick <username>");
                        }

                    }
                    }
                }

                return true;
            }

            public string Help()
            {
                return Name;
            }
        }
    }
}
