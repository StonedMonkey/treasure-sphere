﻿using BlubLib.DotNetty.Handlers.MessageHandling;
using ExpressMapper.Extensions;
using TreasureSphere.Game.GameRules;
using TreasureSphere.Network.Data.Game;
using TreasureSphere.Network.Message.Game;
using TreasureSphere.Network.Message.GameRule;
using ProudNetSrc.Handlers;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace TreasureSphere.Network.Services
{
    internal class RoomService : ProudMessageHandler
    {
        // ReSharper disable once InconsistentNaming
        private static readonly ILogger Logger =
            Log.ForContext(Constants.SourceContextPropertyName, nameof(RoomService));

        [MessageHandler(typeof(GameAvatarDurabilityDecreaseReqMessage))]
        public void GameAvatarDurabilityDecreaseReq(GameSession session, GameAvatarDurabilityDecreaseReqMessage message)
        {
        }

        [MessageHandler(typeof(RoomInfoRequestReqMessage))]
        public void RoomInfoRequestReq(GameSession session, RoomInfoRequestReqMessage message)
        {
            var plr = session.Player;
            var room = plr.Channel.RoomManager[message.RoomId];
            if (room == null)
                return;
            session.SendAsync(new RoomInfoRequestAckMessage()
            {
                Info = new RoomInfoRequestDto()
                {
                    MasterName = room.Master.Account.Nickname,
                    MasterLevel = room.Master.Level,
                    ScoreLimit = room.Options.ScoreLimit,
                    TimeLimit = room.Options.TimeLimit,
                    State = room.GameState,
                    IsMasterInClan = false, //Todo
                    Unk8 = 1,
                    Unk9 = 1
                }
            });
        }

        [MessageHandler(typeof(RoomBeginRoundReq2Message))]
        public void CBeginRoundReq2(GameSession session)
        {
            var plr = session.Player;
            plr?.Room?.BeginRound(plr);
        }

        [MessageHandler(typeof(RoomReadyRoundReq2Message))]
        public void CReadyRoundReq2(GameSession session)
        {
            var plr = session.Player;
            plr?.Room?.ChangeReadyStatus(plr);
        }

        [MessageHandler(typeof(RoomEnterPlayerReqMessage))]
        public void CEnterPlayerReq(GameSession session)
        {
            var plr = session.Player;
            if (plr.Room == null)
            {
                session.SendAsync(new ServerResultAckMessage(ServerResult.CannotFindRoom));
                return;
            }

            plr.RoomInfo.IsConnecting = false;
            plr.Room.OnPlayerJoined(new RoomPlayerEventArgs(plr));


            plr.Room.ChangeMasterIfNeeded(plr);
            plr.Room.ChangeHostIfNeeded(plr);

            session.SendAsync(new GameChangeStateAckMessage(plr.Room.GameState));
            session.SendAsync(new GameChangeSubStateAckMessage(plr.Room.SubGameState));
            
            plr.Room.Broadcast(new RoomEnterPlayerAckMessage()
            {
                AccountId = plr.Account.Id,
                Team = plr.RoomInfo.Team.Team,
                PlayerGameMode = plr.RoomInfo.Mode,
                //Exp = plr.TotalExperience,
                Nickname = plr.Account.Nickname,
                Unk1 = 0,
                //Unk2 = 0
            });

            if (plr.Club != null)
            {
                plr.Room.Broadcast(new RoomEnterClubInfoAckMessage(new PlayerClubInfoDto()
                {
                    Id = plr.Club.Clan_ID,
                    Name = plr.Club.Clan_Name,
                    Type = plr.Club.Clan_Icon,
                }));
            }

            var ClubList = new List<PlayerClubInfoDto>();
            foreach (var player in plr.Room.Players.Values.Where(p => p.Club != null))
            {
                if (!ClubList.Any(club => club.Id == player.Club.Clan_ID))
                {
                    ClubList.Add(new PlayerClubInfoDto()
                    {
                        Id = player.Club.Clan_ID,
                        Name = player.Club.Clan_Name,
                        Type = player.Club.Clan_Icon,
                    });
                }
            }

            plr.Session.SendAsync(new RoomPlayerInfoListForEnterPlayerAckMessage(plr.Room.Players.Values.Select(r => r.Map<Player, RoomPlayerDto>()).ToArray()));
            plr.Session.SendAsync(new RoomClubInfoListForEnterPlayerAckMessage(ClubList.ToArray()));

            if (plr != plr.Room.Master)
                plr.Session.SendAsync(new RoomChangeMasterAckMessage(plr.Room.Master.Account.Id));
            if (plr != plr.Room.Host)
                plr.Session.SendAsync(new RoomChangeRefereeAckMessage(plr.Room.Host.Account.Id));
            
            plr.Room.BroadcastBriefing();

            if (plr.Session.UnreliablePing > 800)
                plr.Session.SendAsync(new ServerResultAckMessage(ServerResult.InternetSlow));
        }

        [MessageHandler(typeof(RoomMakeReqMessage))]
        public void CMakeRoomReq(GameSession session, RoomMakeReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room != null)
            {
                session.SendAsync(new ServerResultAckMessage(ServerResult.ImpossibleToEnterRoom));
                return;
            }
            Logger.Information("CreateRoom || Nickname:{nick}({accid}) Room: {mode}, {mapid}", plr.Account.Nickname,
                plr.Account.Id, (GameRule)message.GameRule, message.Map_ID);
            if (!plr.Channel.RoomManager.GameRuleFactory.Contains((GameRule)message.GameRule))
            {
                Logger.ForAccount(plr)
                    .Error("Game rule {gameRule} does not exist", (GameRule)message.GameRule);
                session.SendAsync(new ServerResultAckMessage(ServerResult.FailedToRequestTask));
                return;
            }

            var israndom = false;
            if ((GameRule)message.GameRule != GameRule.Practice &&
                (GameRule)message.GameRule != GameRule.CombatTrainingTD &&
                (GameRule)message.GameRule != GameRule.CombatTrainingDM)
            {
                if (message.Map_ID == 228 && (GameRule)message.GameRule == GameRule.BattleRoyal) //random br
                {
                    israndom = true;
                    message.Map_ID = 112;
                }
                else if (message.Map_ID == 229 && (GameRule)message.GameRule == GameRule.Chaser) //random chaser
                {
                    israndom = true;
                    message.Map_ID = 225;
                }
                else if (message.Map_ID == 231 && (GameRule)message.GameRule == GameRule.Deathmatch
                ) //random deathmatch
                {
                    israndom = true;
                    message.Map_ID = 20;
                }
                else if (message.Map_ID == 230 && (GameRule)message.GameRule == GameRule.Touchdown) //random touchdown
                {
                    israndom = true;
                    message.Map_ID = 66;
                }
                var map = GameServer.Instance.ResourceCache.GetMaps().GetValueOrDefault(message.Map_ID);
                if (map == null)
                {
                    Logger.ForAccount(plr)
                        .Error("Map {map} does not exist", message.Map_ID);
                    session.SendAsync(new ServerResultAckMessage(ServerResult.FailedToRequestTask));
                    return;
                }
                if (!map.GameRules.Contains((GameRule)message.GameRule))
                {
                    Logger.ForAccount(plr)
                        .Error("Map {mapId}({mapName}) is not available for game rule {gameRule}", map.Id, map.Name,
                            (GameRule)message.GameRule);

                    session.SendAsync(new ServerResultAckMessage(ServerResult.FailedToRequestTask));
                    return;
                }
            }
            if (message.Player_Limit < 1)
                message.Player_Limit = 1;

            if ((GameRule)message.GameRule == GameRule.CombatTrainingTD ||
                (GameRule)message.GameRule == GameRule.CombatTrainingDM)
                message.Player_Limit = 12;

            var matchkey = new MatchKey();

            var isfriendly = true;
            if ((GameRule)message.GameRule == GameRule.CombatTrainingDM ||
                (GameRule)message.GameRule == GameRule.CombatTrainingTD ||
                (GameRule)message.GameRule == GameRule.Practice)
                isfriendly = true;


            var isbalanced = true; //true
            var isburning = false;
            var isWithoutStats = false;
            
            var room = plr.Channel.RoomManager.Create(new RoomCreationOptions
            {
                Name = message.rName,
                GameRule = (GameRule)message.GameRule,
                PlayerLimit = message.Player_Limit,
                TimeLimit = TimeSpan.FromMinutes(message.Time),
                ScoreLimit = (ushort)message.Points,
                Password = message.rPassword,
                IsFriendly = isfriendly,
                IsBalanced = isbalanced,
                IsBurning = isburning,
                IsWithoutStats = isWithoutStats,
                MapID = message.Map_ID,
                MinLevel = 0,
                MaxLevel = 100,
                ItemLimit = (byte)message.Weapon_Limit,
                IsNoIntrusion = false,
                Spectator = message.SpectatorCount,
                IsRandom = israndom,
                hasSpectator = message.SpectatorCount > 1 ? true : false,
                UniqueID = message.Unk3,
                ServerEndPoint = new IPEndPoint(IPAddress.Parse(Config.Instance.IP), Config.Instance.RelayListener.Port),
                Creator = plr
            }, RelayServer.Instance.P2PGroupManager.Create(true));
            
            try
            {
                room.Join(plr);
                plr.Channel?.RoomManager._rooms.TryAdd(room.Id, room);
            }
            catch (RoomAccessDeniedException)
            {
                session.SendAsync(new ServerResultAckMessage(ServerResult.CantEnterRoom));
            }
            catch (RoomLimitReachedException)
            {
                session.SendAsync(new ServerResultAckMessage(ServerResult.CantEnterRoom));
            }
            catch (RoomException)
            {
                session.SendAsync(new ServerResultAckMessage(ServerResult.ImpossibleToEnterRoom));
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                session.SendAsync(new ServerResultAckMessage(ServerResult.ImpossibleToEnterRoom));
            }
        }
        

        [MessageHandler(typeof(RoomChoiceMasterChangeReqMessage))]
        public void RoomChoiceMasterChangeReq(GameSession session, RoomChoiceMasterChangeReqMessage message)
        {
            var plr = session.Player;
            if (plr != plr.Room.Master)
                return;

            var targetplayer = GameServer.Instance.PlayerManager.FirstOrDefault(target =>
                target.Room == plr.Room && target.Account.Id == message.AccountId);
            if (targetplayer == null)
            {
                plr.Session.SendAsync(new ServerResultAckMessage(ServerResult.FailedToRequestTask));
                return;
            }

            plr.Room.ChangeMasterIfNeeded(targetplayer, true);
            plr.Room.ChangeHostIfNeeded(targetplayer, true);
        }
        

        [MessageHandler(typeof(InGamePlayerResponseReqMessage))]
        public void InGamePlayerResponseReq(GameSession session, InGamePlayerResponseReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room != null && plr.RoomInfo?.State != PlayerState.Lobby)
                plr.RoomInfo.State = PlayerState.Alive;
            //Todo
        }
        
        [MessageHandler(typeof(RoomEnterReqMessage))]
        public void CGameRoomEnterReq(GameSession session, RoomEnterReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room != null)
            {
                session.SendAsync(new ServerResultAckMessage(ServerResult.ImpossibleToEnterRoom));
                return;
            }
            var room = plr.Channel.RoomManager[message.RoomId];
            if (room == null)
            {
                Logger.ForAccount(plr)
                    .Error("Room {roomId} in channel {channelId} not found", message.RoomId, plr.Channel.Id);
                session.SendAsync(new ServerResultAckMessage(ServerResult.ImpossibleToEnterRoom));
                return;
            }

            if (room.IsChangingRules)
            {
                session.SendAsync(new ServerResultAckMessage(ServerResult.RoomChangingRules));
                return;
            }

            if (!string.IsNullOrEmpty(room.Options.Password) && !room.Options.Password.Equals(message.Password))
            {
                if(plr.Account.SecurityLevel >= SecurityLevel.GameMaster)
                {
                    room.Join(plr);
                }
                else
                { 
                session.SendAsync(new ServerResultAckMessage(ServerResult.PasswordError));
                return;
                }
            }

            try
            {
                room.Join(plr);
            }
            catch (RoomAccessDeniedException)
            {
                session.SendAsync(new ServerResultAckMessage(ServerResult.CantEnterRoom));
            }
            catch (RoomLimitReachedException)
            {
                session.SendAsync(new ServerResultAckMessage(ServerResult.CantEnterRoom));
            }
            catch (RoomException)
            {
                session.SendAsync(new ServerResultAckMessage(ServerResult.ImpossibleToEnterRoom));
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                session.SendAsync(new ServerResultAckMessage(ServerResult.ImpossibleToEnterRoom));
            }
        }

        [MessageHandler(typeof(RoomLeaveReqMessage))]
        public void CJoinTunnelInfoReq(GameSession session)
        {
            var plr = session.Player;
            plr?.Room?.Leave(plr);
        }

        [MessageHandler(typeof(RoomTeamChangeReqMessage))]
        public void CChangeTeamReq(GameSession session, RoomTeamChangeReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || plr.Room.GameRuleManager.GameRule.StateMachine.State != GameRuleState.Waiting)
                return;

            try
            {
                plr.Room.TeamManager.ChangeTeam(plr, message.Team);
            }
            catch (RoomException ex)
            {
                Logger.ForAccount(plr)
                    .Error(ex, "Failed to change team to {team}", message.Team);
            }
        }

        [MessageHandler(typeof(RoomPlayModeChangeReqMessage))]
        public void CPlayerGameModeChangeReq(GameSession session, RoomPlayModeChangeReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || plr.Room.GameRuleManager.GameRule.StateMachine.State != GameRuleState.Waiting)
                return;
            try
            {
                plr.Room.TeamManager.ChangeMode(plr, message.Mode);
            }
            catch (RoomException ex)
            {
                Logger.ForAccount(plr)
                    .Error(ex, "Failed to change mode to {mode}", message.Mode);
            }
        }
        
        [MessageHandler(typeof(GameLoadingSuccessReqMessage))]
        public void CLoadingSucceeded(GameSession session)
        {
            var plr = session.Player;
            if (plr.Room == null)
                return;

            plr.RoomInfo.hasLoaded = true;
            plr.RoomInfo.State = plr.RoomInfo.Mode == PlayerGameMode.Spectate ? PlayerState.Spectating : PlayerState.Waiting;
            plr.Room.Broadcast(new RoomGameEndLoadingAckMessage(plr.Account.Id));

            if (!plr.Room.hasStarted)
            {
                plr.Room.BroadcastBriefing();
                return;
            }
            else
            {
                plr.RoomInfo.State = plr.RoomInfo.Mode == PlayerGameMode.Spectate ? PlayerState.Spectating : PlayerState.Alive;
            }
            
            plr.Room.GameRuleManager.GameRule.IntrudeCompleted(plr);
        }

        [MessageHandler(typeof(RoomIntrudeRoundReqMessage))]
        public void CIntrudeRoundReq(GameSession session)
        {
            var plr = session.Player;
            if (plr.Room == null || plr.Room.GameRuleManager.GameRule.StateMachine.State == GameRuleState.Waiting)
                return;
            if (plr.Room.IsChangingRules)
            {
                session.SendAsync(new GameEventMessageAckMessage(GameEventMessage.RoomModeIsChanging, 0, 0, 0, ""));
                return;
            }
            if (plr.Room.isPreparing || !plr.Room.hasStarted)
            {
                session.SendAsync(new GameEventMessageAckMessage(GameEventMessage.CantStartGame, 0, 0, 0, ""));
                return;
            }

            plr.Session.SendAsync(new RoomGameLoadingAckMessage());
        }
        
        [MessageHandler(typeof(RoomBeginRoundReqMessage))]
        public void CBeginRoundReq(GameSession session)
        {
            var plr = session.Player;
            if (plr.Room == null || plr.Room.GameRuleManager.GameRule.StateMachine.State != GameRuleState.Waiting)
                return;

            if (plr.Room.IsChangingRules)
            {
                session.SendAsync(new GameEventMessageAckMessage(GameEventMessage.RoomModeIsChanging, 0, 0, 0, ""));
                return;
            }

            var stateMachine = plr.Room.GameRuleManager.GameRule.StateMachine;

            if (stateMachine.CanFire(GameRuleStateTrigger.StartPrepare))
                stateMachine.Fire(GameRuleStateTrigger.StartPrepare);
            else
                session.SendAsync(new GameEventMessageAckMessage(GameEventMessage.CantStartGame, 0, 0, 0, ""));
        }

        [MessageHandler(typeof(RoomReadyRoundReqMessage))]
        public void CReadyRoundReq(GameSession session)
        {
            var plr = session.Player;

            if (plr.Room == null || plr.Room.GameRuleManager.GameRule.StateMachine.State != GameRuleState.Waiting)
                return;

            if (plr.Room.IsChangingRules)
            {
                session.SendAsync(new GameEventMessageAckMessage(GameEventMessage.RoomModeIsChanging, 0, 0, 0, ""));
                return;
            }
            if (plr.Room.GameRuleManager.GameRule.StateMachine.State != GameRuleState.Waiting)
            {
                session.SendAsync(new GameEventMessageAckMessage(GameEventMessage.CantStartGame, 0, 0, 0, ""));
            }
            else
            {
                plr.RoomInfo.IsReady = !plr.RoomInfo.IsReady;
                plr.Room.Broadcast(new RoomReadyRoundAckMessage(plr.Account.Id, plr.RoomInfo.IsReady));
            }
        }
        
        [MessageHandler(typeof(GameEventMessageReqMessage))]
        public void CEventMessageReq(GameSession session, GameEventMessageReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null)
                return;

            plr.Room.Broadcast(new GameEventMessageAckMessage(message.Event, session.Player.Account.Id, message.Unk1,
                message.Value, ""));

            if (!plr.Room.GameRuleManager.GameRule.StateMachine.IsInState(GameRuleState.Playing) /*||
                plr.RoomInfo.State != PlayerState.Lobby*/)
                return;

            //if(plr.RoomInfo.State == PlayerState.Lobby)
            //    if (plr.Room.hasStarted && !plr.RoomInfo.hasLoaded)
            //    {
            //        plr.Session.SendAsync(new RoomGameLoadingAckMessage());
            //        return;
            //    }
            //
            //plr.RoomInfo.State = plr.RoomInfo.Mode == PlayerGameMode.Normal
            //    ? PlayerState.Alive
            //    : PlayerState.Spectating;

            //plr.Room.BroadcastBriefing();
        }


        [MessageHandler(typeof(RoomItemChangeReqMessage))]
        public void CItemsChangeReq(GameSession session, RoomItemChangeReqMessage message)
        {
            var plr = session.Player;
            plr.Room.Broadcast(new RoomChangeItemAckMessage(message.Unk1, message.Unk2));
        }

        [MessageHandler(typeof(GameAvatarChangeReqMessage))]
        public void CAvatarChangeReq(GameSession session, GameAvatarChangeReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room.GameRuleManager.GameRule.StateMachine.State != GameRuleState.HalfTime && 
                plr.RoomInfo.hasLoaded)
            {
                session.SendAsync(new ServerResultAckMessage(ServerResult.FailedToRequestTask));
                return;
            }

            plr.Room.Broadcast(new GameAvatarChangeAckMessage(message.Unk1, message.Unk2));
        }

        [MessageHandler(typeof(RoomChangeRuleNotifyReqMessage))]
        public void CChangeRuleNotifyReq(GameSession session, RoomChangeRuleNotifyReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room.GameRuleManager.GameRule.StateMachine.State != GameRuleState.Waiting)
            {
                session.SendAsync(new ServerResultAckMessage(ServerResult.FailedToRequestTask));
                return;
            }
            try
            {
                session.Player.Room.ChangeRules(message.Settings);
            }
            catch (Exception)
            {
                session.SendAsync(new RoomChangeRuleFailAckMessage { Result = 1 });
            }
        }
        
        [MessageHandler(typeof(RoomLeaveReguestReqMessage))]
        public void CLeavePlayerRequestReq(GameSession session, RoomLeaveReguestReqMessage message)
        {
            var plr = session.Player;
            var room = plr.Room;

            if (message.Reason == RoomLeaveReason.ModeratorKick || message.Reason == RoomLeaveReason.Kicked)
            {
                // Only the master can kick people and kick is only allowed in the lobby
                if ((room.Master != plr || plr.Account.SecurityLevel < SecurityLevel.User) &&
                    !room.GameRuleManager.GameRule.StateMachine.IsInState(GameRuleState.Waiting))
                {
                    session.SendAsync(new ServerResultAckMessage(ServerResult.FailedToRequestTask));
                    return;
                }
            }
            else
            {
                // Dont allow any other reasons for now
                return;
            }

            var targetPlr = room.Players.GetValueOrDefault(message.AccountId);
            if (targetPlr == null)
                return;

            room.Leave(targetPlr, message.Reason);
        }


        [MessageHandler(typeof(RoomQuickJoinReqMessage))]
        public async Task QuickJoinReq(GameSession session, RoomQuickJoinReqMessage message)
        {
            try
            {
                var rooms = new Dictionary<Room, int>();

                foreach (var room in session.Player.Channel.RoomManager)
                {
                    if (room.Options.Password == "")
                    {
                        if (!room.Options.GameRule.Equals((GameRule)message.GameRule))
                            continue;

                        if (!room.GameRuleManager.GameRule.StateMachine.IsInState(GameRuleState.Waiting) &&
                            (!room.GameRuleManager.GameRule.StateMachine.IsInState(GameRuleState.Playing) ||
                             room.Options.IsNoIntrusion))
                            continue;

                        var priority = 0;
                        priority += System.Math.Abs(room.TeamManager[Team.Alpha].Players.Count() -
                                                    room.TeamManager[Team.Beta].Players
                                                        .Count()); // Calculating team balance

                        if (room.GameRuleManager.GameRule.StateMachine.IsInState(GameRuleState.SecondHalf))
                        {
                            if (room.Options.TimeLimit.TotalSeconds / 2 -
                                room.GameRuleManager.GameRule.RoundTime.TotalSeconds <=
                                15) // If only 15 seconds are left...
                            {
                                priority -= 3; // ...lower the room priority
                            }
                        }

                        rooms.Add(room, priority);
                    }
                }
                var roomList = rooms.ToList();

                if (roomList.Any())
                {
                    roomList.Sort((room1, room2) => room2.Value.CompareTo(room1.Value));
                    roomList.First().Key.Join(session.Player);

                    return; // We don't message the Client here, because "Room.Join(...)" already does it.
                }
                await session.SendAsync(new ServerResultAckMessage(ServerResult.CannotFindRoom));
            }
            catch (Exception)
            {
                await session.SendAsync(new ServerResultAckMessage(ServerResult.ServerError));
            }
        }

        [MessageHandler(typeof(TutorialCompletedReqMessage))]
        public void TutorialCompletedReq(GameSession session, TutorialCompletedReqMessage message)
        {
            session.Player.TutorialState = 2;
            session.SendAsync(new TutorialCompletedAckMessage {Unk = 0});
        }

        #region Scores

        [MessageHandler(typeof(ScoreKillReqMessage))]
        public void CScoreKillReq(GameSession session, ScoreKillReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || !plr.Room.hasStarted)
                return;
            
            var room = plr.Room;

            var target = room.Players.GetValueOrDefault(message.Score.Target.AccountId);
            if (target != null)
                target.RoomInfo.PeerId = message.Score.Target;

            var killer = room.Players.GetValueOrDefault(message.Score.Killer.AccountId);
            if (killer != null)
                killer.RoomInfo.PeerId = message.Score.Killer;

            room.GameRuleManager.GameRule.OnScoreKill(killer, null, plr, message.Score.Weapon, message.Score.Target,
                message.Score.Killer, null);
        }

        [MessageHandler(typeof(ScoreKillAssistReqMessage))]
        public void CScoreKillAssistReq(GameSession session, ScoreKillAssistReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || !plr.Room.hasStarted)
                return;

            var room = plr.Room;
            var assist = room.Players.GetValueOrDefault(message.Score.Assist.AccountId);
            if (assist != null)
                assist.RoomInfo.PeerId = message.Score.Assist;

            var killer = room.Players.GetValueOrDefault(message.Score.Killer.AccountId);
            if (killer != null)
                killer.RoomInfo.PeerId = message.Score.Killer;

            room.GameRuleManager.GameRule.OnScoreKill(killer, assist, plr, message.Score.Weapon, message.Score.Target,
                message.Score.Killer, message.Score.Assist);
        }

        [MessageHandler(typeof(ScoreOffenseReqMessage))]
        public void CScoreOffenseReq(GameSession session, ScoreOffenseReqMessage message)
        {
            var plr = session.Player;
            var room = plr.Room;

            if (plr.Room == null || !plr.Room.hasStarted)
                return;

            var target = room.Players.GetValueOrDefault(message.Score.Target.AccountId);
            if (target != null)
                target.RoomInfo.PeerId = message.Score.Target;

            var killer = room.Players.GetValueOrDefault(message.Score.Killer.AccountId);
            if (killer != null)
                killer.RoomInfo.PeerId = message.Score.Killer;

            if (room.Options.GameRule == GameRule.Touchdown)
                ((TouchdownGameRule) room.GameRuleManager.GameRule).OnScoreOffense(killer, null, plr,
                    message.Score.Weapon /*, message.Score.Target, message.Score.Killer, null*/);
            else if (room.Options.GameRule == GameRule.PassTouchdown)
                ((PassTouchdownGameRule) room.GameRuleManager.GameRule).OnScoreOffense(killer, null, plr,
                    message.Score.Weapon /*, message.Score.Target, message.Score.Killer, null*/);
            else if (room.Options.GameRule == GameRule.CombatTrainingTD)
                ((TouchdownTrainingGameRule) room.GameRuleManager.GameRule).OnScoreOffense(killer, null, plr,
                    message.Score.Weapon, message.Score.Target, message.Score.Killer, null);
        }

        [MessageHandler(typeof(ScoreOffenseAssistReqMessage))]
        public void CScoreOffenseAssistReq(GameSession session, ScoreOffenseAssistReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || !plr.Room.hasStarted)
                return;

            var room = plr.Room;

            var assist = room.Players.GetValueOrDefault(message.Score.Assist.AccountId);
            if (assist != null)
                assist.RoomInfo.PeerId = message.Score.Assist;

            var target = room.Players.GetValueOrDefault(message.Score.Target.AccountId);
            if (target != null)
                target.RoomInfo.PeerId = message.Score.Target;

            var killer = room.Players.GetValueOrDefault(message.Score.Killer.AccountId);
            if (killer != null)
                killer.RoomInfo.PeerId = message.Score.Killer;

            if (room.Options.GameRule == GameRule.Touchdown)
                ((TouchdownGameRule) room.GameRuleManager.GameRule).OnScoreOffense(killer, null, plr,
                    message.Score.Weapon /*, message.Score.Target, message.Score.Killer, message.Score.Assist*/);
            else if (room.Options.GameRule == GameRule.PassTouchdown)
                ((PassTouchdownGameRule) room.GameRuleManager.GameRule).OnScoreOffense(killer, null, plr,
                    message.Score.Weapon /*, message.Score.Target, message.Score.Killer, message.Score.Assist*/);
            else if (room.Options.GameRule == GameRule.CombatTrainingTD)
                ((TouchdownTrainingGameRule) room.GameRuleManager.GameRule).OnScoreOffense(killer, null, plr,
                    message.Score.Weapon, message.Score.Target, message.Score.Killer, message.Score.Assist);
        }

        [MessageHandler(typeof(ScoreDefenseReqMessage))]
        public void CScoreDefenseReq(GameSession session, ScoreDefenseReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || !plr.Room.hasStarted)
                return;
            
            var room = plr.Room;

            var target = room.Players.GetValueOrDefault(message.Score.Target.AccountId);
            if (target != null)
                target.RoomInfo.PeerId = message.Score.Target;

            var killer = room.Players.GetValueOrDefault(message.Score.Killer.AccountId);
            if (killer != null)
                killer.RoomInfo.PeerId = message.Score.Killer;

            if (room.Options.GameRule == GameRule.Touchdown)
                ((TouchdownGameRule) room.GameRuleManager.GameRule).OnScoreDefense(killer, null, plr,
                    message.Score.Weapon /*, message.Score.Weapon, message.Score.Target, message.Score.Killer, null*/);
            else if (room.Options.GameRule == GameRule.PassTouchdown)
                ((PassTouchdownGameRule) room.GameRuleManager.GameRule).OnScoreDefense(killer, null, plr,
                    message.Score.Weapon /*, message.Score.Weapon, message.Score.Target, message.Score.Killer, null*/);
            else if (room.Options.GameRule == GameRule.CombatTrainingTD)
                ((TouchdownTrainingGameRule) room.GameRuleManager.GameRule).OnScoreDefense(killer, null, plr,
                    message.Score.Weapon, message.Score.Target, message.Score.Killer, null);
        }

        [MessageHandler(typeof(ScoreDefenseAssistReqMessage))]
        public void CScoreDefenseAssistReq(GameSession session, ScoreDefenseAssistReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || !plr.Room.hasStarted)
                return;

            var room = plr.Room;

            var assist = room.Players.GetValueOrDefault(message.Score.Assist.AccountId);
            if (assist != null)
                assist.RoomInfo.PeerId = message.Score.Assist;

            var target = room.Players.GetValueOrDefault(message.Score.Target.AccountId);
            if (target != null)
                target.RoomInfo.PeerId = message.Score.Target;

            var killer = room.Players.GetValueOrDefault(message.Score.Killer.AccountId);
            if (killer != null)
                killer.RoomInfo.PeerId = message.Score.Killer;

            if (room.Options.GameRule == GameRule.Touchdown)
                ((TouchdownGameRule) room.GameRuleManager.GameRule).OnScoreDefense(killer, assist, plr,
                    message.Score
                        .Weapon /*, message.Score.Weapon, message.Score.Target, message.Score.Killer, message.Score.Assist*/);
            else if (room.Options.GameRule == GameRule.PassTouchdown)
                ((PassTouchdownGameRule) room.GameRuleManager.GameRule).OnScoreDefense(killer, assist, plr,
                    message.Score
                        .Weapon /*, message.Score.Weapon, message.Score.Target, message.Score.Killer, message.Score.Assist*/);
            else if (room.Options.GameRule == GameRule.CombatTrainingTD)
                ((TouchdownTrainingGameRule) room.GameRuleManager.GameRule).OnScoreDefense(killer, assist, plr,
                    message.Score.Weapon, message.Score.Target, message.Score.Killer, message.Score.Assist);
        }

        [MessageHandler(typeof(ScoreTeamKillReqMessage))]
        public void CScoreTeamKillReq(GameSession session, ScoreTeamKillReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || !plr.Room.hasStarted)
                return;

            var room = plr.Room;

            var target = room.Players.GetValueOrDefault(message.Score.Target.AccountId);
            if (target != null)
                target.RoomInfo.PeerId = message.Score.Target;

            var killer = room.Players.GetValueOrDefault(message.Score.Killer.AccountId);
            if (killer != null)
                killer.RoomInfo.PeerId = message.Score.Killer;

            room.GameRuleManager.GameRule.OnScoreKill(killer, null, plr, message.Score.Weapon, message.Score.Target,
                message.Score.Killer, null);
        }

        [MessageHandler(typeof(ScoreHealAssistReqMessage))]
        public void CScoreHealAssistReq(GameSession session, ScoreHealAssistReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || !plr.Room.hasStarted)
                return;
            var room = plr.Room;

            var target = room.Players.GetValueOrDefault(message.Id.AccountId);
            if (target != null)
                target.RoomInfo.PeerId = message.Id;

            room.GameRuleManager.GameRule.OnScoreHeal(plr, message.Id);
        }

        [MessageHandler(typeof(ScoreSuicideReqMessage))]
        public void CScoreSuicideReq(GameSession session, ScoreSuicideReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || !plr.Room.hasStarted)
                return;
            var room = plr.Room;

            var target = room.Players.GetValueOrDefault(message.Id.AccountId);
            if (target != null)
                target.RoomInfo.PeerId = message.Id;

            plr.RoomInfo.PeerId = message.Id;

            room.GameRuleManager.GameRule.OnScoreSuicide(plr, message.Id);
        }

        [MessageHandler(typeof(ScoreReboundReqMessage))]
        public void CScoreReboundReq(GameSession session, ScoreReboundReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || !plr.Room.hasStarted)
                return;

            var room = plr.Room;
            
            var oldPlr = room.Players.GetValueOrDefault(message.OldId.AccountId);
            if (oldPlr != null)
                oldPlr.RoomInfo.PeerId = message.OldId;

            var newPlr = room.Players.GetValueOrDefault(message.NewId.AccountId);
            if (newPlr != null)
                newPlr.RoomInfo.PeerId = message.NewId;


            if (room.Options.GameRule == GameRule.Touchdown)
                ((TouchdownGameRule) room.GameRuleManager.GameRule).OnScoreRebound(newPlr, oldPlr, message.NewId, message.OldId);
            else if (room.Options.GameRule == GameRule.PassTouchdown)
                ((PassTouchdownGameRule) room.GameRuleManager.GameRule).OnScoreRebound(newPlr, oldPlr, message.NewId, message.OldId);
            else if (room.Options.GameRule == GameRule.CombatTrainingTD)
                ((TouchdownTrainingGameRule) room.GameRuleManager.GameRule).OnScoreRebound(newPlr, oldPlr,
                    message.NewId, message.OldId);
        }

        [MessageHandler(typeof(ScoreGoalReqMessage))]
        public void CScoreGoalReq(GameSession session, ScoreGoalReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || !plr.Room.hasStarted)
                return;
            var room = plr.Room;

            var target = room.Players.GetValueOrDefault(message.PeerId.AccountId);
            if (target != null)
                target.RoomInfo.PeerId = message.PeerId;

            if (room.Options.GameRule == GameRule.Touchdown)
                ((TouchdownGameRule) room.GameRuleManager.GameRule).OnScoreGoal(target);
            else if (room.Options.GameRule == GameRule.PassTouchdown)
                ((PassTouchdownGameRule) room.GameRuleManager.GameRule).OnScoreGoal(target);
            else if (room.Options.GameRule == GameRule.CombatTrainingTD)
                ((TouchdownTrainingGameRule) room.GameRuleManager.GameRule).OnScoreGoal(target, message.PeerId);
        }

        [MessageHandler(typeof(SlaughterAttackPointReqMessage))]
        public void SlaughterAttackPointReq(GameSession session, SlaughterAttackPointReqMessage message)
        {
            //session.SendAsync(new SlaughterAttackPointAckMessage()
            //{
            //    Unk1 = message.Unk1,
            //    Unk2 = message.Unk2,
            //    AccountId = message.AccountId,
            //});
            return;
        }
        [MessageHandler(typeof(SlaughterHealPointReqMessage))]
        public void SlaughterHealPointReq(GameSession session, SlaughterHealPointReqMessage message)
        {
            //session.SendAsync(new SlaughterHealPointReqMessage()
            //{
            //    Unk = message.Unk,
            //});
            return;
        }
        
        [MessageHandler(typeof(ScoreMissionScoreReqMessage))]
        public void ScoreMissionScoreReq(GameSession session, ScoreMissionScoreReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || plr.Room.GameRuleManager.GameRule.GameRule != GameRule.Practice)
                return;

            ((PracticeBriefing) plr.Room.GameRuleManager.GameRule.Briefing).Kills = message.Score;
            ((PracticePlayerRecord) plr.Room.GameRuleManager.GameRule.GetPlayerRecord(plr)).Kills = (uint)message.Score;
            session.SendAsync(new ScoreMissionScoreAckMessage() {AccountId = session.Player.Account.Id, Score = message.Score});
            return;
        }
        #endregion

        [MessageHandler(typeof(ArcadeStageFailedReqMessage))]
        public void ArcadeStageFailedReq(GameSession session, ArcadeStageFailedReqMessage message)
        {
            var plr = session.Player;
            if(plr.Room == null || plr.Room.GameRuleManager.GameRule.GameRule != GameRule.Horde)
                return;

            plr.Room.GameRuleManager.GameRule.StateMachine.Fire(GameRuleStateTrigger.StartResult);
        }

        [MessageHandler(typeof(InGameItemGetReqMessage))]
        public void InGameItemGetReq(GameSession session, InGameItemGetReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || plr.Room.GameRuleManager.GameRule.GameRule != GameRule.Horde)
                return;
            session.SendAsync(new InGameItemGetAckMessage()
            {
                Unk1 = (long)plr.Account.Id,
                Unk2 = message.Unk1,
                Unk3 = message.Unk2
            });
        }
        [MessageHandler(typeof(InGameItemDropReqMessage))]
        public void InGameItemDropReq(GameSession session, InGameItemDropReqMessage message)
        {
            var plr = session.Player;
            if (plr.Room == null || plr.Room.GameRuleManager.GameRule.GameRule != GameRule.Horde)
                return;
        }
        
    }
}
