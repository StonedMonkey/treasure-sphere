﻿using System.Collections.Generic;
using BlubLib.DotNetty.Handlers.MessageHandling;
using TreasureSphere.Network.Data.Club;
using TreasureSphere.Network.Data.Game;
using TreasureSphere.Network.Message.Chat;
using TreasureSphere.Network.Message.Club;
using TreasureSphere.Network.Message.Game;
using ProudNetSrc.Handlers;
using Serilog;
using Serilog.Core;
using System;
using System.Linq;
using System.Threading.Tasks;
using ExpressMapper.Extensions;
using TreasureSphere.Network.Data.Chat;
using ProudNetSrc;
using TreasureSphere.Database.Game;
using Dapper.FastCrud;
using TreasureSphere.Database.Auth;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TreasureSphere.Network.Services
{
    internal class ClubService : ProudMessageHandler
    {
        // ReSharper disable once InconsistentNaming
        private static readonly ILogger Logger =
            Log.ForContext(Constants.SourceContextPropertyName, nameof(ClubService));

        public static async Task Update(GameSession session = (GameSession) null, bool broadcast = false)
        {
            if (session == null && broadcast == false)
                return;
            var targets = new List<GameSession>();
            if (broadcast)
                foreach (var sessionsValue in GameServer.Instance.Sessions.Values)
                    targets.Add((GameSession)sessionsValue);
            else
                targets.Add(session);


            foreach (var proudSession in targets)
            {
                var plr = proudSession?.Player;

                if (plr != null)
                {
                    plr.Club = GameServer.Instance.ClubManager.GetClubByAccount(plr.Account.Id);
                    await proudSession?.SendAsync(new Network.Message.Game.ClubInfoAckMessage(plr.Map<Player, PlayerClubInfoDto>()));
                    await proudSession?.SendAsync(new ClubMyInfoAckMessage(plr.Map<Player, MyInfoDto>()));
                }
            }
        }

        //[MessageHandler(typeof(Message.Club.ClubInfoReq2Message))]
        //public void ClubInfoReq2(GameSession session, Message.Club.ClubInfoReq2Message message)
        //{
        //    session.SendAsync(new ServerResultAckMessage(ServerResult.CantReadClanInfo));
        //    //var plr = session.Player;
        //    //
        //    //if (plr.Club != null)
        //    //{
        //    //    var x = plr.Club.Players.Values.Where(p => p.IsMod).FirstOrDefault();
        //    //    string nick = "";
        //    //    if (x != null)
        //    //        nick = x.account.Nickname;
        //    //
        //    //    session.SendAsync(new Message.Club.ClubInfoAck2Message(new ClubSearchInfoDto()
        //    //    {
        //    //        ID = plr.Club.Clan_ID,
        //    //        Name = plr.Club.Clan_Name,
        //    //        Type = plr.Club.Clan_Icon,
        //    //        MasterName = nick,
        //    //        MemberCount = plr.Club.Count,
        //    //        CreationDate = DateTimeOffset.UtcNow.ToString("yyyyMMddHHmmss") //Todo
        //    //    }));
        //    //}
        //    //else
        //    //{
        //    //    session.SendAsync(new ServerResultAckMessage(ServerResult.CantReadClanInfo));
        //    //}
        //}

        [MessageHandler(typeof(Message.Club.ClubInfoReqMessage))]
        public void ClubInfoReq(GameSession session, Message.Club.ClubInfoReqMessage message)
        {
            session.SendAsync(new ServerResultAckMessage(ServerResult.CantReadClanInfo));
            var plr = session.Player;
            
            if (plr.Club != null)
            {
                var x = plr.Club.Players.Values.Where(p => p.IsMod).FirstOrDefault();
                string nick = "";
                if (x != null)
                    nick = x.account.Nickname;
            
                session.SendAsync(new Message.Club.ClubInfoAckMessage(new ClubSearchInfoDto()
                {
                    ID = plr.Club.Clan_ID,
                    Name = plr.Club.Clan_Name,
                    Type = plr.Club.Clan_Icon,
                    MasterName = nick,
                    MemberCount = plr.Club.Count +5, //For Clanmarks
                    CreationDate = DateTimeOffset.UtcNow.ToString("yyyyMMddHHmmss") //Todo
                }));
            }
            else
            {
                session.SendAsync(new Message.Club.ClubInfoAckMessage(new ClubSearchInfoDto()));
            }
        }

        [MessageHandler(typeof(Message.Game.ClubInfoReqMessage))]
        public void ClubInfoReq(GameSession session, Message.Game.ClubInfoReqMessage message)
        {
            var plr = session.Player;
            session.SendAsync(new Message.Game.ClubInfoAckMessage(plr.Map<Player, PlayerClubInfoDto>()));
        }

        [MessageHandler(typeof(ClubSearchReqMessage))]
        public void CClubAddressReq(GameSession session, ClubSearchReqMessage message)
        {
            session.SendAsync(new ClubSearchAckMessage(0, Array.Empty<ClubSearchInfoDto>()));
            //session.SendAsync(new ClubSearchAckMessage(0, GameServer.Instance.ClubManager.Select(c => c.Map<Club, ClubSearchInfoDto>()).ToArray()));
        }

        [MessageHandler(typeof(ClubMemberListReqMessage))]
        public void ClubMemberListReq(ChatSession session, ClubMemberListReqMessage message)
        {
            //TODO
            //Wrong Struct
            //if(session.Player.Club != null)
            //    session.SendAsync(new ClubMemberListAckMessage(GameServer.Instance.PlayerManager.Where(p =>
            //        session.Player.Club.Players.Keys.Contains(p.Account.Id)).Select(p => p.Map<Player, ClubMemberDto>()).ToArray()));
            session.GameSession?.SendAsync(new ServerResultAckMessage(ServerResult.CantReadClanInfo));
        }

        [MessageHandler(typeof(ClubNameCheckReqMessage))]
        public void ClubNameCheckReq(GameSession session, ClubNameCheckReqMessage message)
        {
            if (GameServer.Instance.ClubManager.Any(c => c.Clan_Name == message.Unk))
                session.SendAsync(new ClubNameCheckAckMessage(2));
            else
                session.SendAsync(new ClubNameCheckAckMessage(0));
        }

        [MessageHandler(typeof(ClubCreateReqMessage))]
        public async Task ClubCreateReq(GameSession session, ClubCreateReqMessage message)
        {
            if (GameServer.Instance.ClubManager.Any(c => c.Clan_Name == message.Unk1) || session.Player.Club != null)
                await session.SendAsync(new ClubCreateAckMessage(1));
            else
            {
                ClubDto club = new ClubDto()
                {
                    Name = message.Unk2,
                    Icon = ""
                };
                
                using (var db = GameDatabase.Open())
                {
                    try
                    {
                        using (var transaction = db.BeginTransaction())
                        {
                            AccountDto playeracc = (db.Find<AccountDto>(statement => statement
                           .Where($"{nameof(AccountDto.Id):C} = @Id")
                           .WithParameters(new { session.Player.Account.Id }))).FirstOrDefault();

                            PlayerDto plrdto = (db.Find<PlayerDto>(statement => statement
                           .Where($"{nameof(PlayerDto.Id):C} = @Id")
                           .WithParameters(new { session.Player.Account.Id }))).FirstOrDefault();

                            db.Insert(club, statement => statement.AttachToTransaction(transaction));
                            Club Club = new Club(club, new[] { new ClubPlayerInfo() { AccountId = session.Player.Account.Id, account = playeracc, State = ClubState.Member, IsMod = true } });
                            GameServer.Instance.ClubManager.Add(Club);
                            transaction.Commit();

                            
                            db.Insert(new ClubPlayerDto()
                            {
                                PlayerId = (int)session.Player.Account.Id,
                                ClubId = club.Id,
                                IsMod = true,
                                State = (int)ClubState.Member
                            });

                            session.Player.Club = Club;
                        }
                    }
                    catch(Exception ex)
                    {
                        Logger.Error(ex.ToString());
                        await session.SendAsync(new ClubCreateAckMessage(1));
                        return;
                    }

                    await session.SendAsync(new ClubCreateAckMessage(0));
                    await session.SendAsync(new ClubMyInfoAckMessage(session.Player.Map<Player, MyInfoDto>()));
                    await session.SendAsync(new Message.Game.ClubInfoAckMessage(session.Player.Club.Map<Club, PlayerClubInfoDto>()));
                }
            }
        }

        [MessageHandler(typeof(ClubUnjoinReqMessage))]
        public async Task ClubLeaveMessage(GameSession session, ClubUnjoinReqMessage message)
        {
            ClubDto club = new ClubDto();
            using (var db = GameDatabase.Open())
            {
                try
                {
                    using (var transaction = db.BeginTransaction())
                    {
                        AccountDto playeracc = (db.Find<AccountDto>(statement => statement
                           .Where($"{nameof(AccountDto.Id):C} = @Id")
                           .WithParameters(new { session.Player.Account.Id }))).FirstOrDefault();

                        PlayerDto plrdto = (db.Find<PlayerDto>(statement => statement
                       .Where($"{nameof(PlayerDto.Id):C} = @Id")
                       .WithParameters(new { session.Player.Account.Id }))).FirstOrDefault();
                        transaction.Commit();
                        db.Delete(new ClubPlayerDto()
                        {
                            PlayerId = (int)session.Player.Account.Id,
                            ClubId = club.Id,
                            IsMod = true || false,
                            State = (int)ClubState.Member
                        });


                    }
                }
                catch(Exception ex)
                {
                    Logger.Error(ex.ToString());
                    await session.SendAsync(new ClubUnjoinAckMessage());
                    return;
                }
                await session.SendAsync(new ClubUnjoinAckMessage());
                await session.SendAsync(new ClubMyInfoAckMessage(session.Player.Map<Player, MyInfoDto>()));
                await session.SendAsync(new Message.Game.ClubInfoAckMessage(session.Player.Club.Map<Club, PlayerClubInfoDto>()));
            }

        }


        [MessageHandler(typeof(ClubRankListReqMessage))]
        public void ClubRankListReq(GameSession session, ClubRankListReqMessage message)
        {
            session.SendAsync(new ServerResultAckMessage(ServerResult.CantReadClanInfo));
        }

        [MessageHandler(typeof(ClubAddressReqMessage))]
        public void CClubAddressReq(GameSession session, ClubAddressReqMessage message)
        {
            Logger.ForAccount(session)
                .Debug("ClubAddressReq: {message}", message);

            session.SendAsync(new ClubAddressAckMessage("", 0));
        }

    }
}
