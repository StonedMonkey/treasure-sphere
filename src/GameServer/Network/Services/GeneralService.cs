﻿using System;
using BlubLib.DotNetty.Handlers.MessageHandling;
using TreasureSphere.Network.Data.Club;
using TreasureSphere.Network.Message.Chat;
using TreasureSphere.Network.Message.Club;
using TreasureSphere.Network.Message.Game;
using ProudNetSrc.Handlers;

namespace TreasureSphere.Network.Services
{
    internal class GeneralService : ProudMessageHandler
    {
        [MessageHandler(typeof(TimeSyncReqMessage))]
        public void TimeSyncHandler(GameSession session, TimeSyncReqMessage message)
        {
            session.SendAsync(new TimeSyncAckMessage
            {
                ClientTime = message.Time,
                ServerTime = (uint) Program.AppTime.ElapsedMilliseconds
            });
        }
        [MessageHandler(typeof(CheckhashKeyvaluereqMessage))]
        public void CheckhashKeyvaluereq(GameSession session, CheckhashKeyvaluereqMessage message)
        {
            //handle
        }
        
    }
}
